# Benchmark HashSet<int> vs IntSet

## Test environment

```
BenchmarkDotNet v0.13.7, Manjaro Linux
12th Gen Intel Core i7-12700H, 1 CPU, 20 logical and 14 physical cores
.NET SDK 6.0.416
  [Host]     : .NET Core 3.1.32 (CoreCLR 4.700.22.55902, CoreFX 4.700.22.56512), X64 RyuJIT AVX2
  DefaultJob : .NET Core 3.1.32 (CoreCLR 4.700.22.55902, CoreFX 4.700.22.56512), X64 RyuJIT AVX2
```

## Results

| Method                                |       Mean |     Error |    StdDev |
|:--------------------------------------|-----------:|----------:|----------:|
| CreateWithInitCollection_HashSet<int> |   8.175 ms | 0.1217 ms | 0.1079 ms |
| CreateWithInitCollection_IntSet       |   3.654 ms | 0.0224 ms | 0.0199 ms |
| ------                                |            |           |           |
| Add_HashSet<int>                      |  13.052 ms | 0.2435 ms | 0.3644 ms |
| Add_IntSet                            |   5.205 ms | 0.0854 ms | 0.1198 ms |
| ------                                |            |           |           |
| Remove_HashSet<int>                   | 3,385.4 us |  66.90 us | 144.01 us |
| Remove_IntSet                         |   736.7 us |  14.65 us |  31.23 us |
| ------                                |            |           |           |
| Has_HashSet<int>                      | 4,818.1 us |  96.12 us | 220.84 us |
| Has_IntSet                            |   588.4 us |  11.75 us |  28.38 us |
| ------                                |            |           |           |
| Iterate_HashSet<int>                  | 2,774.2 us |  55.39 us |  86.24 us |
| Iterate_IntSet                        |   693.4 us |  13.81 us |  29.73 us |
| ------                                |            |           |           |
| IterateFirstAndLast_HashSet<int>      |  27.523 ms | 0.5390 ms | 0.5535 ms |
| IterateFirstAndLast_IntSet            |   6.805 ms | 0.1356 ms | 0.1507 ms |



