using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;
using EntitasRedux.Core.Libs.Collections;

namespace EntitasRedux.Core.Libs.Benchmark
{
	public class IntSetBenchmark
	{
		private static readonly int[] ValuesCollection = Enumerable.Repeat(0, 1000000).ToArray();

		[BenchmarkCategory("HashSet")]
		public class Create
		{
			[Benchmark]
			public void Create_Standard()
			{
				var hashSet = new HashSet<int>();
			}

			[Benchmark]
			public void Create_Optimized()
			{
				var intSet = IntSet.Create();
			}
		}

		[BenchmarkCategory("HashSet")]
		public class CreateWithInitCollection
		{
			[Benchmark]
			public void CreateWithInitCollection_Standard()
			{
				var hashSet = new HashSet<int>(ValuesCollection);
			}

			[Benchmark]
			public void CreateWithInitCollection_Optimized()
			{
				var intHashSet = IntSet.Create(ValuesCollection);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Add
		{
			private HashSet<int> _hashSet;
			private IntSet _intSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>();
				_intSet = IntSet.Create();
			}

			[Benchmark]
			public void Add_Standard()
			{
				for (var i = 0; i < 1000000; i++)
					_hashSet.Add(i);
			}

			[Benchmark]
			public void Add_Optimized()
			{
				for (var i = 0; i < 1000000; i++)
					_intSet.Add(i);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Remove
		{
			private HashSet<int> _hashSet;
			private IntSet _intSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_intSet = IntSet.Create(ValuesCollection);
			}

			[Benchmark]
			public void Remove_Standard()
			{
				foreach (var value in ValuesCollection)
					_hashSet.Remove(value);
			}

			[Benchmark]
			public void Remove_Optimized()
			{
				foreach (var value in ValuesCollection)
					_intSet.Remove(value);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Has
		{
			private HashSet<int> _hashSet;
			private IntSet _intSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_intSet = IntSet.Create(ValuesCollection);
			}

			[Benchmark]
			public void Has_Standard()
			{
				foreach (var value in ValuesCollection)
					_hashSet.Contains(value);
			}

			[Benchmark]
			public void Has_Optimized()
			{
				foreach (var value in ValuesCollection)
					_intSet.Contains(value);
			}
		}

		[BenchmarkCategory("HashSet")]
		public class Iteration
		{
			private HashSet<int> _hashSet;
			private IntSet _intSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_intSet = IntSet.Create(ValuesCollection);
			}

			[Benchmark]
			public void Iterate_Standard()
			{
				for (int i = 0; i < 1000000; i++)
				{
					foreach (var value in _hashSet)
					{
					}
				}
			}

			[Benchmark]
			public void Iterate_Optimized()
			{
				for (int i = 0; i < 1000000; i++)
				{
					foreach (var value in _intSet.AsSpan())
					{
					}
				}
			}
		}

		[BenchmarkCategory("HashSet")]
		public class IterationFirstAndLast
		{
			private HashSet<int> _hashSet;
			private IntSet _intSet;

			[IterationSetup]
			public void IterationSetup()
			{
				_hashSet = new HashSet<int>(ValuesCollection);
				_hashSet.Clear();
				_hashSet.Add(ValuesCollection[0]);
				_hashSet.Add(ValuesCollection[^1]);

				_intSet = IntSet.Create(ValuesCollection);
				_intSet.Clear();
				_intSet.Add(ValuesCollection[0]);
				_intSet.Add(ValuesCollection[^1]);
			}

			[Benchmark]
			public void IterateFirstAndLast_Standard()
			{
				for (int i = 0; i < 10000000; i++)
				{
					foreach (var value in _hashSet)
					{
					}
				}
			}

			[Benchmark]
			public void IterateFirstAndLast_Optimized()
			{
				for (int i = 0; i < 10000000; i++)
				{
					foreach (var value in _intSet.AsSpan())
					{
					}
				}
			}
		}
	}
}