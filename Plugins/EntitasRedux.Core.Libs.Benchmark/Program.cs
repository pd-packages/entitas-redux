using BenchmarkDotNet.Running;

namespace EntitasRedux.Core.Libs.Benchmark
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			BenchmarkRunner.Run(new[]
				{
					typeof(IntSetBenchmark.Create),
					typeof(IntSetBenchmark.CreateWithInitCollection),
					typeof(IntSetBenchmark.Add),
					typeof(IntSetBenchmark.Remove),
					typeof(IntSetBenchmark.Has),
					typeof(IntSetBenchmark.Iteration),
					typeof(IntSetBenchmark.IterationFirstAndLast),
				}
			);
		}
	}
}