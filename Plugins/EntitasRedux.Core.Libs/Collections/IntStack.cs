using System;
using System.Runtime.CompilerServices;

namespace EntitasRedux.Core.Libs.Collections
{
	public struct IntStack
	{
		public Bits Bits;
		public int[] Values;
		public int Count;

		public static IntStack Create(int capacity)
		{
			var bits = Bits.Create(capacity);
			return new IntStack
			{
				Bits = bits,
				Values = new int[bits.Length]
			};
		}
	}

	public static class IntPoolExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Push(this ref IntStack intStack, int value)
		{
			var requiredLength = value + 1;
			if (intStack.Bits.Length < requiredLength)
				intStack.Bits.Resize(requiredLength);

			if (intStack.Count == intStack.Values.Length)
				Array.Resize(ref intStack.Values, intStack.Values.Length << 1);

			intStack.Bits.Set(value);
			intStack.Values[intStack.Count++] = value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Pop(this ref IntStack intStack)
		{
			var value = intStack.Values[--intStack.Count];
			intStack.Bits.Unset(value);
			return value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Has(this ref IntStack intStack, int value)
			=> intStack.Bits.Length > value && intStack.Bits.IsSet(value);
	}
}