using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace EntitasRedux.Core.Libs.Collections
{
	public struct IntSet
	{
		public int[] Elements;
		public int[] Positions;

		public static IntSet Create(int capacity = 32) => new()
		{
			Elements = new int[(int)(capacity * 0.5f)],
			Positions = new int[capacity]
		};

		public static IntSet Create(IEnumerable<int> values)
		{
			var intSet = Create();
			foreach (var value in values)
				intSet.Add(value);

			return intSet;
		}
	}

	public static class IntSetExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Count(this ref IntSet set)
			=> set.Elements[0];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Add(this ref IntSet set, int value)
		{
			if (set.Contains(value))
				return false;

			if (value >= set.Positions.Length)
			{
				var length = set.Positions.Length;
				do
				{
					length <<= 1;
				} while (value >= length);

				Array.Resize(ref set.Positions, length);
			}

			ref var count = ref set.Elements[0];
			count++;

			if (count >= set.Elements.Length)
				Array.Resize(ref set.Elements, set.Elements.Length << 1);

			set.Elements[count] = value;
			set.Positions[value] = count;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Remove(this ref IntSet set, int value)
		{
			if (!set.Contains(value))
				return false;

			ref var count = ref set.Elements[0];
			ref var removedValueIndex = ref set.Positions[value];
			ref var lastValue = ref set.Elements[count];
			set.Elements[removedValueIndex] = lastValue;
			set.Positions[lastValue] = removedValueIndex;
			count--;
			removedValueIndex = 0;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Contains(this ref IntSet set, int value)
			=> set.Positions.Length > value && set.Positions[value] > 0;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Clear(this ref IntSet set)
		{
			set.Elements[0] = 0;
			Array.Clear(set.Positions, 0, set.Positions.Length);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<int> AsSpan(this ref IntSet set)
		{
			ref var count = ref set.Elements[0];
			return new ReadOnlySpan<int>(set.Elements, 1, count);
		}
	}
}