using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace EntitasRedux.Core.Libs.Collections
{
	public readonly struct ImmutableHashSet<T> : IReadOnlyCollection<T>
	{
		public static readonly ImmutableHashSet<T> Empty = new(new HashSet<T>());

		internal readonly HashSet<T> Set;

		private ImmutableHashSet(HashSet<T> set)
		{
			Set = set;
		}

		public int Count => Set.Count;

		public HashSet<T>.Enumerator GetEnumerator() => Set.GetEnumerator();

		IEnumerator<T> IEnumerable<T>.GetEnumerator() => GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		public static implicit operator ImmutableHashSet<T>(HashSet<T> set) => new(set);
	}

	public static class ImmutableHashSetExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ImmutableHashSet<T> ToImmutableHashSet<T>(HashSet<T> set) => set;
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Contains<T>(this in ImmutableHashSet<T> immutableHashSet, T item)
			=> immutableHashSet.Set.Contains(item);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsProperSubsetOf<T>(this in ImmutableHashSet<T> immutableHashSet, IEnumerable<T> other)
			=> immutableHashSet.Set.IsProperSubsetOf(other);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsProperSupersetOf<T>(this in ImmutableHashSet<T> immutableHashSet, IEnumerable<T> other)
			=> immutableHashSet.Set.IsProperSupersetOf(other);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsSubsetOf<T>(this in ImmutableHashSet<T> immutableHashSet, IEnumerable<T> other)
			=> immutableHashSet.Set.IsSubsetOf(other);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsSupersetOf<T>(this in ImmutableHashSet<T> immutableHashSet, IEnumerable<T> other)
			=> immutableHashSet.Set.IsSupersetOf(other);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Overlaps<T>(this in ImmutableHashSet<T> immutableHashSet, IEnumerable<T> other)
			=> immutableHashSet.Set.Overlaps(other);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool SetEquals<T>(this in ImmutableHashSet<T> immutableHashSet, IEnumerable<T> other)
			=> immutableHashSet.Set.SetEquals(other);
	}
}