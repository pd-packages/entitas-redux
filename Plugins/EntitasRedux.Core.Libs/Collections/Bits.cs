using System;
using System.Runtime.CompilerServices;

namespace EntitasRedux.Core.Libs.Collections
{
	public struct Bits
	{
		public int[] Flags;
		public int Length;

		public static Bits Create(int bits)
		{
			var ints = new int[BitsExtensions.GetInt32ArrayLengthFromBitLength(bits)];
			return new Bits
			{
				Flags = ints,
				Length = ints.Length * 32
			};
		}
	}

	public static class BitsExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int GetInt32ArrayLengthFromBitLength(int length)
			=> ((length - 1) >> 5) + 1;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsSet(this ref Bits bits, int index)
			=> (bits.Flags[index >> 5] & 1 << index) != 0;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Set(this ref Bits bits, int index)
			=> bits.Flags[index >> 5] |= 1 << index;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Unset(this ref Bits bits, int index)
			=> bits.Flags[index >> 5] &= ~(1 << index);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Resize(this ref Bits bits, int length)
		{
			bits.Length = length;
			var int32ArrayLength = GetInt32ArrayLengthFromBitLength(length);
			Array.Resize(ref bits.Flags, int32ArrayLength);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int GetSetBitsCount(this ref Bits bits)
		{
			var count = 0;

			for (var i = 0; i < bits.Flags.Length; i++)
				count += HammingWeight(bits.Flags[i]);

			return count;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int GetUnsetBitsCount(this ref Bits bits)
		{
			var count = bits.Length;

			for (var i = 0; i < bits.Flags.Length; i++)
				count -= HammingWeight(bits.Flags[i]);

			return count;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static int HammingWeight(int v)
		{
			v -= (v >> 1) & 0x55555555;
			v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
			return ((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
		}
	}
}