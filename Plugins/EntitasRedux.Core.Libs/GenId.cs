using System.Collections.Generic;

namespace EntitasRedux.Core.Libs
{
	public struct GenId
	{
		public int Index;
		public int CreationIndex;

		public GenId(int index) : this()
		{
			Index = index;
		}

		public GenId(int index, int creationIndex)
		{
			Index = index;
			CreationIndex = creationIndex;
		}

		public static bool operator ==(GenId a, GenId b)
			=> a.Index == b.Index && a.CreationIndex == b.CreationIndex;

		public static bool operator !=(GenId a, GenId b) => !(a == b);

		public bool Equals(GenId other) => Index == other.Index && CreationIndex == other.CreationIndex;

		public override bool Equals(object obj) => obj is GenId other && Equals(other);

		public override int GetHashCode() => Index.GetHashCode();

		public override string ToString() => $"{nameof(Index)}: {Index}, {nameof(CreationIndex)}: {CreationIndex}";


		public static readonly IEqualityComparer<GenId> Comparer = new IndexCreationIndexEqualityComparer();

		private sealed class IndexCreationIndexEqualityComparer : IEqualityComparer<GenId>
		{
			public bool Equals(GenId x, GenId y) => x.Index == y.Index && x.CreationIndex == y.CreationIndex;

			public int GetHashCode(GenId obj) => obj.Index.GetHashCode();
		}
	}
}