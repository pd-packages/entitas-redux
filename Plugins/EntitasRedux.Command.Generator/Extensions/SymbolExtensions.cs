using System.Linq;
using Microsoft.CodeAnalysis;

namespace EntitasRedux.Command.Generator.Extensions
{
	public static class SymbolExtensions
	{
		public static bool IsCommand(this INamedTypeSymbol symbol)
		{
			foreach (var attributeData in symbol.GetAttributes())
			{
				var attributeClass = attributeData.AttributeClass;
				if (attributeClass == null)
					continue;

				var nameWithNamespace = attributeClass.GetNameWithNamespace();
				if (nameWithNamespace == "JCMG.EntitasRedux.Commands.CommandAttribute")
					return true;
			}

			return false;
		}

		public static string GetNamespace(this ITypeSymbol typeSymbol)
			=> typeSymbol.ContainingNamespace == null || string.IsNullOrEmpty(typeSymbol.ContainingNamespace.Name)
				? string.Empty
				: typeSymbol.ContainingNamespace.ToDisplayString();

		public static string GetNameWithNamespace(this ITypeSymbol typeSymbol)
		{
			var ns = typeSymbol.GetNamespace();
			return string.IsNullOrEmpty(ns) ? typeSymbol.Name : ns + "." + typeSymbol.Name;
		}

		public static string GetFullName(this ITypeSymbol typeSymbol)
		{
			if (typeSymbol is IArrayTypeSymbol arrayTypeSymbol)
				return arrayTypeSymbol.ElementType.GetFullName() + "[]";

			var name = typeSymbol.GetNameWithNamespace();
			if (typeSymbol is not INamedTypeSymbol namedTypeSymbol)
				return name;

			return namedTypeSymbol.IsGenericType
				? $"{name}<{string.Join(", ", namedTypeSymbol.TypeArguments.Select(s => s.GetFullName()))}>"
				: name;
		}
	}
}