using System.Collections.Generic;

namespace EntitasRedux.Command.Generator.Extensions
{
	public static class StringExtensions
	{
		public static string ToMethodName(this string s)
			=> s.EndsWith("Cmd") ? s.Substring(0, s.Length - 3) : s;

		public static string ToArgName(this string s)
		{
			var chars = s.ToCharArray();
			chars[0] = char.ToLower(chars[0]);
			return new string(chars);
		}

		public static string Join(this IEnumerable<string> enumerable, string separator)
			=> string.Join(separator, enumerable);
	}
}