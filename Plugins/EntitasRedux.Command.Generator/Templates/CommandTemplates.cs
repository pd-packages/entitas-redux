using System.Linq;
using EntitasRedux.Command.Generator.Data;
using EntitasRedux.Command.Generator.Extensions;

namespace EntitasRedux.Command.Generator.Templates
{
	public static class CommandTemplates
	{
		public static string CreateClass(CommandGenerationData data)
			=> $@"
using System.Runtime.CompilerServices;

public static partial class CommandBufferExtensions
{{
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
{CreateMethod(data)}
}}
";

		private static string CreateMethod(CommandGenerationData data)
			=> @$"	public static void Create{data.Name.ToMethodName()}(
		{(data.Fields.Length == 0
			? "this JCMG.EntitasRedux.Commands.ICommandBuffer buffer"
			: $"this JCMG.EntitasRedux.Commands.ICommandBuffer buffer,\n{CreateArgs(data)}")}
	)
	{{
{CreateBody(data)}
	}}";

		private static string CreateArgs(CommandGenerationData data)
			=> data.Fields.Select(s => $"		{s.TypeSymbol.GetFullName()} {s.Name.ToArgName()}")
				.Join(",\n");

		private static string CreateBody(CommandGenerationData data) => data.Fields.Length != 0
			? $"		ref var cmd = ref buffer.Create<{data.GetFullTypeName()}>();\n{CreateFieldSetter(data)}"
			: $"		buffer.Create<{data.GetFullTypeName()}>();";

		private static string CreateFieldSetter(CommandGenerationData data)
			=> data.Fields.Select(s => $"		cmd.{s.Name} = {s.Name.ToArgName()};")
				.Join("\n");
	}
}