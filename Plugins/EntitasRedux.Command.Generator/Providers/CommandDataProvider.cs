using System.Collections.Generic;
using EntitasRedux.Command.Generator.Data;
using EntitasRedux.Command.Generator.Extensions;
using Microsoft.CodeAnalysis;

namespace EntitasRedux.Command.Generator.Providers
{
	public class CommandDataProvider
	{
		public static CommandGenerationData? GetData(INamedTypeSymbol symbol)
		{
			if (!symbol.IsCommand())
				return null;

			var fields = new List<CommandFieldData>();
			foreach (var member in symbol.GetMembers())
			{
				if (member is not IFieldSymbol fieldSymbol)
					continue;

				fields.Add(new CommandFieldData
				{
					Name = fieldSymbol.Name,
					TypeSymbol = fieldSymbol.Type
				});
			}

			return new CommandGenerationData
			{
				Namespace = symbol.GetNamespace(),
				Name = symbol.Name,
				Fields = fields.ToArray()
			};
		}
	}
}