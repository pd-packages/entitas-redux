﻿using System.Threading;
using EntitasRedux.Command.Generator.Providers;
using EntitasRedux.Command.Generator.Templates;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EntitasRedux.Command.Generator
{
	[Generator]
	public class CommandExtensionGenerator : IIncrementalGenerator
	{
		public void Initialize(IncrementalGeneratorInitializationContext context)
		{
			var provider = context.SyntaxProvider.CreateSyntaxProvider(GetSyntaxNode, GetSemantic);
			context.RegisterSourceOutput(provider, Generate);
		}

		private static bool GetSyntaxNode(SyntaxNode node, CancellationToken token)
		{
			if (node is not StructDeclarationSyntax syntax)
				return false;

			foreach (var attributeList in syntax.AttributeLists)
			foreach (var attributeSyntax in attributeList.Attributes)
			{
				var attrName = attributeSyntax.Name.GetText().ToString();
				if (attrName.Contains("Command"))
					return true;
			}

			return false;
		}

		private static INamedTypeSymbol GetSemantic(GeneratorSyntaxContext context, CancellationToken _)
			=> context.SemanticModel.GetDeclaredSymbol(context.Node) as INamedTypeSymbol;

		private static void Generate(SourceProductionContext context, INamedTypeSymbol symbol)
		{
			var data = CommandDataProvider.GetData(symbol);
			if (!data.HasValue)
				return;

			var content = CommandTemplates.CreateClass(data.Value);
			context.AddSource(data.Value.GetFullTypeName() + ".g.cs", content);
		}
	}
}