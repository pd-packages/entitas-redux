using Microsoft.CodeAnalysis;

namespace EntitasRedux.Command.Generator.Data
{
	public struct CommandFieldData
	{
		public string Name;
		public ITypeSymbol TypeSymbol;
	}
}