namespace EntitasRedux.Command.Generator.Data
{
	public struct CommandGenerationData
	{
		public string Namespace;
		public string Name;
		public CommandFieldData[] Fields;

		public string GetFullTypeName() => !string.IsNullOrEmpty(Namespace)
			? Namespace + "." + Name
			: Name;
	}
}