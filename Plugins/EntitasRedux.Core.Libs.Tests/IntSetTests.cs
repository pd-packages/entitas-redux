using System;
using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Libs.Collections;
using Xunit;

namespace EntitasRedux.Core.Libs.Tests
{
	public class IntSetTests
	{
		[Fact]
		public void AddRange_ContainRange_RemoveRange()
		{
			const int range = 64;

			var intSet = IntSet.Create(range);
			for (var i = 0; i < range; i++)
				Assert.True(intSet.Add(i));

			Assert.Equal(range, intSet.Count());

			for (var i = 0; i < range; i++)
				Assert.True(intSet.Contains(i));

			var readOnlySpan = intSet.AsSpan();
			for (var i = 0; i < range; i++)
				Assert.Equal(i, readOnlySpan[i]);

			for (var i = 0; i < range; i++)
				Assert.True(intSet.Remove(i));

			for (var i = 0; i < range; i++)
				Assert.False(intSet.Contains(i));

			Assert.Equal(0, intSet.Count());
		}

		[Fact]
		public void DuplicatesAreNotAdded()
		{
			var intSet = IntSet.Create();
			Assert.True(intSet.Add(16));
			Assert.True(intSet.Contains(16));
			Assert.Equal(1, intSet.Count());

			Assert.False(intSet.Add(16));
			Assert.Equal(1, intSet.Count());
		}

		[Fact]
		public void SetClear()
		{
			const int range = 64;

			var intSet = IntSet.Create(range);
			for (var i = 0; i < range; i++)
				Assert.True(intSet.Add(i));

			intSet.Clear();

			Assert.Equal(0, intSet.Elements[0]);
			for (var i = 0; i < intSet.Positions.Length; i++)
				Assert.Equal(0, intSet.Positions[i]);

			Assert.Equal(0, intSet.Count());
		}

		[Fact]
		public void DataValidation()
		{
			var random = new Random();
			var expectedSet = new HashSet<int>();
			var intSet = IntSet.Create();

			for (var i = 0; i < 1_000; i++)
			{
				var operations = random.Next(0, 10);
				var operation = random.Next(2);
				for (var j = 0; j < operations; j++)
				{
					var value = random.Next(0, 1024);
					var result = operation switch
					{
						0 => expectedSet.Add(value) == intSet.Add(value),
						1 => expectedSet.Remove(value) == intSet.Remove(value),
						_ => throw new ArgumentOutOfRangeException()
					};

					Assert.True(result, $"Unexpected operation '{(operation == 1 ? "Add" : "Remove")}' result");
				}

				Assert.Equal(expectedSet.Count, intSet.Count());

				var span = intSet.AsSpan();
				Assert.Equal(expectedSet.Count, span.Length);

				var groupBy = span.ToArray().GroupBy(s => s);
				foreach (var group in groupBy)
					Assert.Single(group);

				foreach (var v in span)
					Assert.Contains(v, expectedSet);
			}
		}

		[Fact]
		public void RemoveMiddleElementFromSet()
		{
			var intSet = IntSet.Create();
			intSet.Add(1);
			intSet.Add(2);
			intSet.Add(3);
			intSet.Remove(2);

			Assert.Equal(2, intSet.Positions[3]);
		}
		
		[Fact]
		public void ResizeSet()
		{
			var intSet = IntSet.Create();
			intSet.Add(64);
		}
	}
}