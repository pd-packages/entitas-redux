using EntitasRedux.Core.Libs.Collections;
using Xunit;

namespace EntitasRedux.Core.Libs.Tests
{
	public class BitsTest
	{
		[Fact]
		public void SetAndUnsetCorrect2()
		{
			const int size = 32;

			var bits = Bits.Create(size);

			Assert.Single(bits.Flags);

			for (var i = 0; i < size; i++)
			{
				var flag = 1 << i;

				bits.Set(i);
				Assert.True(bits.IsSet(i));
				Assert.Equal(flag, bits.Flags[0]);

				bits.Unset(i);
				Assert.False(bits.IsSet(i));
				Assert.Equal(0, bits.Flags[0]);
			}
		}

		[Fact]
		public void SetAndUnsetCorrect()
		{
			const int size = 64;

			var bits = Bits.Create(size);

			Assert.Equal(2, bits.Flags.Length);

			for (var i = 0; i < size; i++)
				bits.Set(i);

			for (var i = 0; i < size; i++)
				Assert.True(bits.IsSet(i));

			for (var i = 0; i < size; i++)
				bits.Unset(i);

			for (var i = 0; i < size; i++)
				Assert.False(bits.IsSet(i));
		}

		[Fact]
		public void ResizeAndNextValuesCopied()
		{
			const int size = 64;
			const int nextSize = 128;

			var bits = Bits.Create(size);

			Assert.Equal(2, bits.Flags.Length);
			Assert.Equal(64, bits.Length);

			for (var i = 0; i < size; i++)
				bits.Set(i);

			bits.Resize(nextSize);

			Assert.Equal(4, bits.Flags.Length);
			Assert.Equal(128, bits.Length);

			for (var i = 0; i < nextSize; i++)
			{
				if (i < size)
					Assert.True(bits.IsSet(i));
				else
					Assert.False(bits.IsSet(i));
			}
		}
	}
}