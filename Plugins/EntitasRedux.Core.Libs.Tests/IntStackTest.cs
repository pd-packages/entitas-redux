using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Libs.Collections;
using Xunit;

namespace EntitasRedux.Core.Libs.Tests
{
	public class IntStackTest
	{
		[Fact]
		public void Test()
		{
			var ints = Enumerable.Range(0, 32).ToArray();
			var intPool = IntStack.Create(32);

			for (var i = 0; i < ints.Length; i++)
			{
				intPool.Push(ints[i]);
			}

			Assert.Equal(ints.Length, intPool.Count);

			var free = new List<int>();
			for (var i = 0; i < ints.Length; i++)
			{
				free.Add(intPool.Pop());
			}

			Assert.Equal(0, intPool.Count);

			free.Sort();

			Assert.Equal(ints, free);
			Assert.Equal(0, intPool.Count);

			for (var i = 0; i < ints.Length; i++)
			{
				Assert.False(intPool.Has(ints[i]));
			}
		}

		[Fact]
		public void PushAndFreeSignleValue()
		{
			var intPool = IntStack.Create(32);

			intPool.Push(1);
			var free = intPool.Pop();
			Assert.Equal(1, free);

			Assert.False(intPool.Has(1));
		}
	}
}