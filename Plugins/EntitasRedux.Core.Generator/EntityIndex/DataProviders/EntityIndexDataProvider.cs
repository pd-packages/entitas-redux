﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Generator.Generator;
using Microsoft.CodeAnalysis;

namespace EntitasRedux.Core.Generator
{
	internal sealed class EntityIndexDataProvider : IDataProvider
	{
		public CodeGeneratorData[] GetData(IEnumerable<ICachedNamedTypeSymbol> namedTypeSymbols)
		{
			var componentName = CoreTypes.IComponent;

			var entityIndexData = namedTypeSymbols
				.Where(cachedNamedTypeSymbol => !cachedNamedTypeSymbol.NamedTypeSymbol.IsAbstract)
				.Where(cachedNamedTypeSymbol => cachedNamedTypeSymbol.ImplementsInterface(componentName))
				.ToDictionary(
					cachedNamedTypeSymbol => cachedNamedTypeSymbol,
					cachedNamedTypeSymbol => cachedNamedTypeSymbol.GetPublicMemberData())
				.Where(kv => kv.Value.Any(info =>
				{
					var hasIndexAttr = info.memberFieldSymbol != null &&
					                   info.memberFieldSymbol.GetAttributes()
						                   .HasAttribute(CoreTypes.AbstractEntityIndexAttribute,
							                   canInherit: true);
					return hasIndexAttr;
				}))
				.SelectMany(kv => CreateEntityIndexData(kv.Key, kv.Value));

			var customEntityIndexData = namedTypeSymbols
				.Where(cachedNamedTypeSymbol => !cachedNamedTypeSymbol.NamedTypeSymbol.IsAbstract)
				.Where(cachedNamedTypeSymbol
					=> cachedNamedTypeSymbol.HasAttribute(CoreTypes.CustomEntityIndexAttribute))
				.Select(CreateCustomEntityIndexData);

			return entityIndexData
				.Concat(customEntityIndexData)
				.ToArray();
		}

		private static EntityIndexData[] CreateEntityIndexData(
			ICachedNamedTypeSymbol cachedNamedTypeSymbol,
			IEnumerable<MemberData> memberData
		)
		{
			var hasMultiple = memberData
				.Count(i => i.memberFieldSymbol.GetAttributes()
					.HasAttribute(CoreTypes.AbstractEntityIndexAttribute, canInherit: true)) > 1;

			return memberData
				.Where(i => i.memberFieldSymbol.GetAttributes()
					.HasAttribute(CoreTypes.AbstractEntityIndexAttribute, canInherit: true))
				.Select(
					info =>
					{
						var data = new EntityIndexData();
						var attribute = info.memberFieldSymbol.GetAttributes()
							.GetAttributes(CoreTypes.AbstractEntityIndexAttribute, canInherit: true)
							.Single();

						data.SetEntityIndexType(GetEntityIndexType(attribute));
						data.IsCustom(false);
						data.SetEntityIndexName(cachedNamedTypeSymbol.FullTypeName.ToComponentName());
						data.SetKeyType(info.compilableTypeString);
						data.SetComponentType(cachedNamedTypeSymbol.FullTypeName);
						data.SetMemberName(info.name);
						data.SetHasMultiple(hasMultiple);
						data.SetContextNames(cachedNamedTypeSymbol.NamedTypeSymbol.GetContextNames());

						return data;
					})
				.ToArray();
		}

		private static EntityIndexData CreateCustomEntityIndexData(ICachedNamedTypeSymbol cachedNamedTypeSymbol)
		{
			var data = new EntityIndexData();
			var attribute = cachedNamedTypeSymbol.GetAttributes(CoreTypes.CustomEntityIndexAttribute).Single();
			var contextType = (ITypeSymbol)attribute.ConstructorArguments[0].Value;

			var fullTypeName = cachedNamedTypeSymbol.FullTypeName;
			data.SetEntityIndexType(fullTypeName);
			data.IsCustom(true);
			data.SetEntityIndexName(fullTypeName.RemoveDots());
			data.SetHasMultiple(false);
			data.SetContextNames(
				new[]
				{
					contextType.GetFullTypeName().ShortTypeName().RemoveContextSuffix()
				});

			var getMethods = contextType.GetAllMembers()
				.Where(s => s.HasAttribute(CoreTypes.EntityIndexGetMethodAttribute))
				.Select(
					method => new MethodData(
						method.GetReturnType().GetFullTypeName(),
						method.Name,
						method.GetParameters()
							.Select(p => new MemberData(p.Type.GetFullTypeName(), p.Name))
							.ToArray()))
				.ToArray();

			data.SetCustomMethods(getMethods);

			return data;
		}

		private static string GetEntityIndexType(AttributeData attribute)
		{
			return attribute.AttributeClass.Name switch
			{
				CoreTypes.EntityIndexAttribute => "JCMG.EntitasRedux.EntityIndex",
				CoreTypes.PrimaryEntityIndexAttribute => "JCMG.EntitasRedux.PrimaryEntityIndex",
				_ => throw new Exception(
					$"Unhandled EntityIndex Type: {attribute.AttributeClass.GetFullTypeName()}")
			};
		}
	}
}