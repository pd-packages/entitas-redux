﻿using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Generator.Generator;

namespace EntitasRedux.Core.Generator
{
	/// <summary>
	/// A <see cref="IDataProvider"/> that creates a collection of <see cref="ContextData"/> per user-defined context.
	/// </summary>
	internal sealed class ContextDataProvider : IDataProvider
	{
		public CodeGeneratorData[] GetData(IEnumerable<ICachedNamedTypeSymbol> namedTypeSymbols)
		{
			return namedTypeSymbols
				.Where(s => s.IsBaseType(CoreTypes.ContextAttribute))
				.Select(CreateContextData)
				.Cast<CodeGeneratorData>()
				.ToArray();
		}

		/// <summary>
		/// Creates an instance of <see cref="ContextData"/> for <paramref name="namedTypeSymbol"/>.
		/// </summary>
		private static ContextData CreateContextData(ICachedNamedTypeSymbol namedTypeSymbol)
		{
			const string attributeSuffix = "Attribute";

			var symbol = namedTypeSymbol.NamedTypeSymbol;
			var symbolName = symbol.Name;
			var contextName = symbolName;
			if (contextName.EndsWith(attributeSuffix))
				contextName = contextName.Substring(0, contextName.Length - attributeSuffix.Length);

			var contextNamespace = symbol.ContainingNamespace.ToDisplayString();

			var data = new ContextData();
			data.SetContextFullName(symbolName);
			data.SetContextName(string.IsNullOrEmpty(contextName) ? symbolName : contextName);
			data.SetContextNamespace(contextNamespace != "<global namespace>" ? contextNamespace : string.Empty);
			return data;
		}
	}
}