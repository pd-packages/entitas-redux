﻿using EntitasRedux.Core.Generator.Generator;

namespace EntitasRedux.Core.Generator
{
	public class ContextData : CodeGeneratorData
	{
		public const string CONTEXT_FULL_NAME = "Context.FullName";
		public const string CONTEXT_NAME = "Context.Name";
		public const string CONTEXT_NAMESPACE = "Context.Namespace";

		public string GetContextFullName() => (string)this[CONTEXT_FULL_NAME];

		public void SetContextFullName(string contextName) => this[CONTEXT_FULL_NAME] = contextName;

		public string GetContextName() => (string)this[CONTEXT_NAME];

		public void SetContextName(string contextName) => this[CONTEXT_NAME] = contextName;

		public string GetContextNamespace() => (string)this[CONTEXT_NAMESPACE];

		public void SetContextNamespace(string value) => this[CONTEXT_NAMESPACE] = value;
	}
}