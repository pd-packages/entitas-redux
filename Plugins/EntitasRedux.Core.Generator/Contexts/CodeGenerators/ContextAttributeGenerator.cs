﻿using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Generator.Generator;
using EntitasRedux.Core.Generator.Tools;

namespace EntitasRedux.Core.Generator
{
	internal sealed class ContextAttributeGenerator : ICodeGenerator
	{
		private const string NAMESPACE_TEMPLATE = @"
${namespace}
{
${content}
}
";

		private const string TEMPLATE = @"
public sealed partial class ${ContextName}
{
	public ${ContextName}() : base(""${contextName}"")	{ }
}
";

		private static CodeGenFile Generate(ContextData data)
		{
			var fullName = data.GetContextFullName();
			var content = TEMPLATE
				.Replace("${ContextName}", fullName)
				.Replace("${contextName}", data.GetContextName());

			var contextNamespace = data.GetContextNamespace();
			content = !string.IsNullOrEmpty(contextNamespace)
				? NAMESPACE_TEMPLATE.Replace("${content}", content)
				: content;

			return new CodeGenFile(fullName.AddSourceExtension(), content);
		}

		public CodeGenFile[] Generate(IEnumerable<CodeGeneratorData> data)
		{
			return data
				.OfType<ContextData>()
				.Select(Generate)
				.ToArray();
		}
	}
}