using System.Collections.Generic;
using EntitasRedux.Core.Generator.Generator;

namespace EntitasRedux.Core.Generator
{
	public static class CodeGenFileExtensions
	{
		public static void Merge(this List<CodeGenFile> files, IEnumerable<CodeGenFile> enumerable)
		{
			foreach (var genFile in enumerable)
			{
				var hasFile = false;

				foreach (var existFile in files)
				{
					if (existFile.FileName != genFile.FileName)
						continue;

					hasFile = true;
					existFile.FileContent += "\n\n" + genFile.FileContent;
					break;
				}

				if (hasFile)
					continue;

				files.Add(genFile);
			}
		}
	}
}