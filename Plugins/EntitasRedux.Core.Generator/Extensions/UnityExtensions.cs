using Microsoft.CodeAnalysis;

namespace EntitasRedux.Core.Generator
{
	/// <summary>
	/// Helper methods for Unity related types.
	/// </summary>
	public static class UnityExtensions
	{
		/// <summary>
		/// Returns true if this <see cref="ITypeSymbol"/> is or derives from a Unity Object, otherwise false.
		/// </summary>
		public static bool IsUnityObject(this ITypeSymbol typeSymbol)
		{
			return typeSymbol.InheritsFromOrIs("UnityEngine.Object");
		}

		/// <summary>
		/// Returns true if this <see cref="ITypeSymbol"/> is or derives from a Unity ScriptableObject, otherwise false.
		/// </summary>
		public static bool IsUnityScriptableObject(this ITypeSymbol typeSymbol)
		{
			return typeSymbol.InheritsFromOrIs("UnityEngine.ScriptableObject");
		}

		/// <summary>
		/// Returns true if this <see cref="ITypeSymbol"/> is or derives from a Unity MonoBehaviour, otherwise false.
		/// </summary>
		public static bool IsUnityMonoBehaviour(this ITypeSymbol typeSymbol)
		{
			return typeSymbol.InheritsFromOrIs("UnityEngine.MonoBehaviour");
		}

		/// <summary>
		/// Returns true if this <see cref="ITypeSymbol"/> is or derives from a Unity GameObject, otherwise false.
		/// </summary>
		public static bool IsUnityGameObject(this ITypeSymbol typeSymbol)
		{
			return typeSymbol.InheritsFromOrIs("UnityEngine.GameObject");
		}
	}
}