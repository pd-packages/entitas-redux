﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EntitasRedux.Core.Generator.Generator;

namespace EntitasRedux.Core.Generator
{
	/// <summary>
	/// Helper methods for <see cref="Type"/>
	/// </summary>
	public static class TypeExtensions
	{
		/// <summary>
		///     This is the set of types from the C# keyword list to their alias.
		/// </summary>
		private static readonly Dictionary<Type, string> TYPE_ALIAS = new Dictionary<Type, string>
		{
			{ typeof(bool), "bool" },
			{ typeof(byte), "byte" },
			{ typeof(char), "char" },
			{ typeof(decimal), "decimal" },
			{ typeof(double), "double" },
			{ typeof(float), "float" },
			{ typeof(int), "int" },
			{ typeof(long), "long" },
			{ typeof(object), "object" },
			{ typeof(sbyte), "sbyte" },
			{ typeof(short), "short" },
			{ typeof(string), "string" },
			{ typeof(uint), "uint" },
			{ typeof(ulong), "ulong" },
			// Yes, this is an odd one.  Technically it's a type though.
			{ typeof(void), "void" }
		};

		/// <summary>
		///     This is the set of types from the C# keyword list to their alias.
		/// </summary>
		private static readonly Dictionary<string, string> STR_TYPE_ALIAS = new Dictionary<string, string>
		{
			{ typeof(bool).FullName, "bool" },
			{ typeof(byte).FullName, "byte" },
			{ typeof(char).FullName, "char" },
			{ typeof(decimal).FullName, "decimal" },
			{ typeof(double).FullName, "double" },
			{ typeof(float).FullName, "float" },
			{ typeof(int).FullName, "int" },
			{ typeof(long).FullName, "long" },
			{ typeof(object).FullName, "object" },
			{ typeof(sbyte).FullName, "sbyte" },
			{ typeof(short).FullName, "short" },
			{ typeof(string).FullName, "string" },
			{ typeof(uint).FullName, "uint" },
			{ typeof(ulong).FullName, "ulong" },
			// Yes, this is an odd one.  Technically it's a type though.
			{ typeof(void).FullName, "void" }
		};

		/// <summary>
		///     Returns true if <typeparamref name="T" /> implements interface <paramref name="type" />.
		/// </summary>
		public static bool ImplementsInterface<T>(this Type type)
		{
			var interfaceType = typeof(T);
			if (!interfaceType.IsInterface)
			{
				throw new ArgumentException("T must be an interface type.");
			}

			var interfaces = type.GetInterfaces();

			return interfaces.Contains(interfaceType);
		}

		/// <summary>
		///     Returns a list of <see cref="PublicMemberInfo" /> instances for all public members on this
		///     <paramref name="type" />.
		/// </summary>
		public static List<PublicMemberInfo> GetPublicMemberInfos(this Type type)
		{
			var fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public);
			var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
			var publicMemberInfoList = new List<PublicMemberInfo>(fields.Length + properties.Length);
			for (var index = 0; index < fields.Length; ++index)
				publicMemberInfoList.Add(new PublicMemberInfo(fields[index]));

			for (var index = 0; index < properties.Length; ++index)
			{
				var info = properties[index];
				if (info.CanRead && info.CanWrite && info.GetIndexParameters().Length == 0)
					publicMemberInfoList.Add(new PublicMemberInfo(info));
			}

			return publicMemberInfoList;
		}

		/// <summary>
		///     Returns a safe-readable version of a short type name, without generic or array characters.
		/// </summary>
		public static string GetHumanReadableName(this Type type)
		{
			var result = type.GetTypeNameOrAlias().UppercaseFirst();
			if (type.IsArray)
			{
				var elementType = type.GetElementType();
				result = string.Format(CodeGenerationConstants.ARRAY_SHORT_NAME, elementType.GetTypeNameOrAlias());
			}
			else if (type.IsGenericType)
			{
				var backTickIndex = result.IndexOf(CodeGenerationConstants.BACKTICK_CHAR);
				if (backTickIndex > 0) result = result.Remove(backTickIndex);

				var genericTypeParameters = type.GetGenericArguments();
				for (var i = 0; i < genericTypeParameters.Length; i++)
					result += genericTypeParameters[i].GetHumanReadableName();
			}

			return result;
		}

		/// <summary>
		///     Returns the short type name or the C# alias for this type.
		/// </summary>
		public static string GetTypeNameOrAlias(this Type type)
		{
			// Lookup alias for type
			if (TYPE_ALIAS.TryGetValue(type, out var alias)) return alias;

			// Default to CLR type name
			return type.Name;
		}

		/// <summary>
		///     Returns the type name or the C# alias for this type.
		/// </summary>
		public static string GetTypeNameOrAlias(this string fullTypeName)
		{
			// Lookup alias for type
			if (STR_TYPE_ALIAS.TryGetValue(fullTypeName, out var alias)) return alias;

			// Default to CLR type name
			return fullTypeName;
		}

		/// <summary>
		///     Returns the full type name for this type.
		/// </summary>
		public static string GetFullTypeName(this Type type)
		{
			var result = type.FullName;
			if (type.IsGenericType)
			{
				var backTickIndex = result.IndexOf(CodeGenerationConstants.BACKTICK_CHAR);
				if (backTickIndex > 0) result = result.Remove(backTickIndex);

				result += "<";
				var genericTypeParameters = type.GetGenericArguments();
				for (var i = 0; i < genericTypeParameters.Length; ++i)
				{
					var typeParamName = genericTypeParameters[i].GetFullTypeName();
					result += i == 0 ? typeParamName : "," + typeParamName;
				}

				result += ">";
			}

			return result.Replace('+', '.');
		}

		/// <summary>
		/// Returns true if <paramref name="type"/> implements either <see cref="IList{T}"/> or
		/// <see cref="IReadOnlyList{T}"/>. If true, <paramref name="genericType"/> will be initialized with the list's
		/// generic type value.
		/// </summary>
		/// <param name="type"></param>
		/// <param name = "genericType"> </param>
		/// <returns></returns>
		public static bool IsList(this Type type, out Type genericType)
		{
			genericType = null;

			var typeToCheck = type;
			while (typeToCheck != null)
			{
				// If not a generic type, continue checking the base type
				if (!typeToCheck.IsGenericType)
				{
					typeToCheck = typeToCheck.BaseType;
					continue;
				}

				// If not a generic list or derived from a generic list, continue checking the base type
				var genericTypeDef = typeToCheck.GetGenericTypeDefinition();
				if (genericTypeDef != typeof(List<>))
				{
					typeToCheck = typeToCheck.BaseType;
					continue;
				}

				genericType = typeToCheck.GetGenericArguments()[0];
				break;
			}

			return genericType != null;
		}

		/// <summary>
		/// Returns true if <paramref name="type"/> is a <see cref="Dictionary{TKey,TValue}"/>, otherwise false. If true,
		/// <paramref name="genericValueType"/> and <paramref name="genericValueType"/> will be initialized to the value
		/// and key type of the dictionary.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="genericKeyType"></param>
		/// <param name="genericValueType"></param>
		/// <returns></returns>
		public static bool IsDictionary(this Type type, out Type genericKeyType, out Type genericValueType)
		{
			genericKeyType = null;
			genericValueType = null;

			var typeToCheck = type;
			while (typeToCheck != null)
			{
				// If not a generic type, continue checking the base type
				if (!typeToCheck.IsGenericType)
				{
					typeToCheck = typeToCheck.BaseType;
					continue;
				}

				// If not a generic list or derived from a generic list, continue checking the base type
				var genericTypeDef = typeToCheck.GetGenericTypeDefinition();
				if (genericTypeDef != typeof(Dictionary<,>))
				{
					typeToCheck = typeToCheck.BaseType;
					continue;
				}

				var genericArguments = typeToCheck.GetGenericArguments();
				genericKeyType = genericArguments[0];
				genericValueType = genericArguments[1];
				break;
			}

			return genericKeyType != null && genericValueType != null;
		}

		/// <summary>
		/// Returns true if <paramref name="type"/> has a default empty constructor, otherwise false.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool HasDefaultConstructor(this Type type)
		{
			return type.GetConstructor(Type.EmptyTypes) != null;
		}

		/// <summary>
		/// Returns true if <paramref name="type"/> is a mutable reference type, otherwise false.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool IsMutableReferenceType(this Type type)
		{
			return !type.IsValueType && type != typeof(string);
		}
	}
}