﻿using System;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp;

namespace EntitasRedux.Core.Generator
{
	public static class CodeGeneratorExtensions
	{
		public const string LOOKUP = "ComponentsLookup";

		private const string KEYWORD_PREFIX = "@";

		public static string ComponentName(this ComponentData data)
		{
			return data.GetTypeName().ToComponentName();
		}

		public static string[] GetComponentNames(this ComponentData data)
		{
			// Attempt to get the actual Type object for this
			var type = data.GetTypeName().ToType();
			if (type == null)
				return new[] { data.GetTypeName() };

			// Otherwise if the attribute is inaccessible return the string-calculated type name.
			return new[]
			{
				type.ToCompilableString().ShortTypeName().AddComponentSuffix()
			};

			// Otherwise just return the type name as originally set in the ComponentData instance.
		}

		public static string ComponentNameValidLowercaseFirst(this ComponentData data)
		{
			return ComponentName(data).LowercaseFirst().AddPrefixIfIsKeyword();
		}

		public static string ComponentNameValidUppercaseFirst(this ComponentData data)
		{
			return ComponentName(data).UppercaseFirst().AddPrefixIfIsKeyword();
		}

		public static string ComponentNameWithContext(this ComponentData data, string contextName)
		{
			return contextName + data.ComponentName();
		}

		public static string ComponentIndex(this ComponentData data, string contextName)
		{
			return contextName + LOOKUP + "." + data.ComponentName();
		}

		public static string ComponentListenerIndex(this ComponentData data, string contextName, EventData eventData)
		{
			var componentListenerName = data.ComponentName() + GetEventTypeSuffix(eventData) + "Listener";
			if (eventData.eventTarget == EventTarget.Any)
				componentListenerName = eventData.eventTarget + componentListenerName;
			if (data.GetContextNames().Length > 1)
				componentListenerName = contextName + componentListenerName;

			return contextName + LOOKUP + "." + componentListenerName;
		}

		public static string EventDelegateName(this ComponentData data, string contextName, EventData eventData)
			=> $"On{contextName}{EventComponentName(data, eventData)}{GetEventTypeSuffix(eventData)}";

		public static string Replace(this string template, string contextName) => template
			.Replace("${ContextName}", contextName)
			.Replace("${contextName}", contextName)
			.Replace("${ContextType}", contextName.AddContextSuffix())
			.Replace("${EntityType}", contextName.AddEntitySuffix())
			.Replace("${MatcherType}", contextName.AddMatcherSuffix())
			.Replace("${Lookup}", contextName + LOOKUP);

		public static string Replace(this string template, ComponentData data, string contextName) => template
			.Replace(contextName)
			.Replace("${ComponentType}", data.GetTypeName())
			.Replace("${ComponentName}", data.ComponentName())
			.Replace("${componentName}", data.ComponentName().UppercaseFirst())
			.Replace("${validComponentName}", data.ComponentNameValidUppercaseFirst())
			.Replace("${prefixedComponentName}", data.PrefixedComponentName())
			.Replace("${newMethodParameters}", GetMethodParameters(data.GetMemberData(), true))
			.Replace("${methodParameters}", GetMethodParameters(data.GetMemberData(), false))
			.Replace("${newMethodArgs}", data.GetMemberData().GetMethodArgs(true))
			.Replace("${methodArgs}", data.GetMemberData().GetMethodArgs(false))
			.Replace("${Index}", data.ComponentIndex(contextName));

		public static string Replace(this string template, ComponentData data, string contextName, EventData eventData)
		{
			var eventListener = data.EventListener(contextName, eventData);
			return template
				.Replace(data, contextName)
				.Replace("${DelegateEventName}", data.EventDelegateName(contextName, eventData))
				.Replace("${EventComponentName}", data.EventComponentName(eventData))
				.Replace("${EventListenerComponent}", eventListener.AddComponentSuffix())
				.Replace("${EventName}", data.EventName(contextName, eventData))
				.Replace("${Event}", data.Event(eventData))
				.Replace("${EventListener}", eventListener)
				.Replace("${eventListener}", eventListener.UppercaseFirst())
				.Replace("${EventType}", GetEventTypeSuffix(eventData));
		}

		public static string Replace(this string template, MemberData memberData) => template
			.Replace("${MemberName}", memberData.name)
			.Replace("${MemberNameUpper}", memberData.name.UppercaseFirst())
			.Replace("${MemberCompilableString}", memberData.compilableTypeString);

		public static string PrefixedComponentName(this ComponentData data)
			=> data.GetFlagPrefix().UppercaseFirst() + data.ComponentName();

		public static string EventName(this ComponentData data, string contextName, EventData eventData)
		{
			var optionalContextName = data.GetContextNames().Length > 1 ? contextName : string.Empty;
			return optionalContextName + data.Event(eventData);
		}

		public static string EventFileName(this ComponentData data, string contextName, EventData eventData)
			=> contextName + data.Event(eventData);

		public static string Event(this ComponentData data, EventData eventData)
			=> EventComponentName(data, eventData) + GetEventTypeSuffix(eventData);

		public static string EventListener(this ComponentData data, string contextName, EventData eventData)
			=> data.EventName(contextName, eventData).AddListenerSuffix();

		public static string EventListenerFileName(this ComponentData data, string contextName, EventData eventData)
			=> contextName + data.Event(eventData).AddListenerSuffix();

		public static string EventComponentName(this ComponentData data, EventData eventData)
		{
			var componentName = data.GetTypeName().ToComponentName();
			var shortComponentName = data.GetTypeName().ToComponentName();
			var eventComponentName = componentName.Replace(
				shortComponentName,
				eventData.GetEventPrefix() + shortComponentName);
			return eventComponentName;
		}

		public static string GetEventMethodArgs(this ComponentData data, EventData eventData, string args)
		{
			if (data.GetMemberData().Length == 0)
			{
				return string.Empty;
			}

			return eventData.eventType == EventType.Removed
				? string.Empty
				: args;
		}

		public static string GetEventTypeSuffix(this EventData eventData)
		{
			switch (eventData.eventType)
			{
				case EventType.Added:
					return string.Empty;
				case EventType.Removed:
					return "Removed";
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static string GetEventPrefix(this EventData eventData)
		{
			switch (eventData.eventTarget)
			{
				case EventTarget.Any:
					return "Any";
				case EventTarget.Self:
					return string.Empty;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static string GetMethodParameters(this MemberData[] memberData, bool newPrefix)
		{
			return string.Join(
				", ",
				memberData
					.Select(
						info => info.compilableTypeString +
						        (newPrefix ? " new" + info.name.UppercaseFirst() : " " + info.name.LowercaseFirst()))
					.ToArray());
		}

		public static string GetMethodArgs(this MemberData[] memberData, bool newPrefix)
		{
			return string.Join(
				", ",
				memberData
					.Select(info => newPrefix ? "new" + info.name.UppercaseFirst() : info.name)
					.ToArray());
		}

		public static string AddPrefixIfIsKeyword(this string name)
		{
			if (SyntaxFacts.GetKeywordKind(name) == SyntaxKind.IsKeyword)
				name = KEYWORD_PREFIX + name;

			return name;
		}
	}
}