using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EntitasRedux.Core.Generator
{
	public static class SyntaxExtensions
	{
		public static string GetTypeName(this BaseTypeSyntax baseTypeSyntax) => baseTypeSyntax.Type switch
		{
			// Assuming it's an identifier name, you can cast it and get the Identifier text.
			IdentifierNameSyntax identifierNameSyntax => identifierNameSyntax.Identifier.Text,
			// If it's a qualified name (e.g., Namespace.TypeName)
			QualifiedNameSyntax qualifiedNameSyntax => qualifiedNameSyntax.ToString(),
			_ => string.Empty
		};
	}
}