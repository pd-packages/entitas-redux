namespace EntitasRedux.Core.Generator
{
	/// <summary>
	/// The access modifier visibility of a symbol.
	/// </summary>
	public enum SymbolVisibility
	{
		Public,
		Internal,
		Private
	}
}