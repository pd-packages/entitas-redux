namespace EntitasRedux.Core.Generator
{
	public struct CoreTypes
	{
		public const string GenId = "EntitasRedux.Core.Libs.GenId";

		public const string IComponent = "IComponent";

		public const string ContextAttributeFullName = "JCMG.EntitasRedux.ContextAttribute";
		public const string ContextAttribute = "ContextAttribute";
		public const string EventAttribute = "EventAttribute";
		public const string CleanupAttribute = "CleanupAttribute";
		public const string DontGenerateAttribute = "DontGenerateAttribute";
		public const string GenerateEntityInterfaceAttribute = "GenerateEntityInterfaceAttribute";
		public const string AbstractEntityIndexAttribute = "AbstractEntityIndexAttribute";
		public const string CustomEntityIndexAttribute = "CustomEntityIndexAttribute";
		public const string EntityIndexGetMethodAttribute = "EntityIndexGetMethodAttribute";
		public const string EntityIndexAttribute = "EntityIndexAttribute";
		public const string PrimaryEntityIndexAttribute = "PrimaryEntityIndexAttribute";
		public const string FlagPrefixAttribute = "FlagPrefixAttribute";
		public const string UniqueAttribute = "UniqueAttribute";
	}
}