namespace EntitasRedux.Core.Generator.Tools
{
	public static class FileTool
	{
		public const string GENERATOR_SOURCE_EXTENSIONS = ".g.cs";

		// public static string CreateName(params string[] args)
		// 	=> string.Join(".", args).AddSourceExtension();

		public static string AddSourceExtension(this string s) => s + GENERATOR_SOURCE_EXTENSIONS;
	}
}