using System.Collections.Generic;

namespace EntitasRedux.Core.Generator.Generator
{
	public interface ICodeGenerator
	{
		CodeGenFile[] Generate(IEnumerable<CodeGeneratorData> data);
	}
}