namespace EntitasRedux.Core.Generator.Generator
{
	/// <summary>
	/// Shared constants for code generation.
	/// </summary>
	public static class CodeGenerationConstants
	{
		public const string ARRAY_SHORT_NAME = "{0}Array";
		public const char BACKTICK_CHAR = '`';
		public const char LEFT_CHEVRON_CHAR = '<';
		public const string GLOBAL_NAMESPACE = "<global namespace>";
	}
}