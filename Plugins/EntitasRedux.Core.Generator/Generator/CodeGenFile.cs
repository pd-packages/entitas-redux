namespace EntitasRedux.Core.Generator.Generator
{
	public sealed class CodeGenFile
	{
		public string FileName { get; set; }

		public string FileContent { get; set; }

		public CodeGenFile(string fileName, string fileContent)
		{
			FileName = fileName;
			FileContent = fileContent;
		}
	}
}