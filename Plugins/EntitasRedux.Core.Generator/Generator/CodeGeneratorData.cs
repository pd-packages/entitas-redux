﻿using System.Collections.Generic;

namespace EntitasRedux.Core.Generator.Generator
{
	public abstract class CodeGeneratorData : Dictionary<string, object>
	{
		protected CodeGeneratorData()
		{
		}

		protected CodeGeneratorData(CodeGeneratorData data) : base(data)
		{
		}
	}
}