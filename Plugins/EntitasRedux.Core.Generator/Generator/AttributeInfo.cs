using System.Collections.Generic;

namespace EntitasRedux.Core.Generator.Generator
{
	/// <summary>
	/// Represents information about an attribute
	/// </summary>
	public class AttributeInfo
	{
		public readonly object Attribute;
		public readonly List<PublicMemberInfo> MemberInfos;

		public AttributeInfo(object attribute, List<PublicMemberInfo> memberInfos)
		{
			Attribute = attribute;
			MemberInfos = memberInfos;
		}
	}
}