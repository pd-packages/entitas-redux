using System.Collections.Generic;

namespace EntitasRedux.Core.Generator.Generator
{
	public interface IDataProvider
	{
		CodeGeneratorData[] GetData(IEnumerable<ICachedNamedTypeSymbol> namedTypeSymbols);
	}
}