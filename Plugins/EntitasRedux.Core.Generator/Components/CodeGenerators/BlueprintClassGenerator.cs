﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Generator.Generator;
using EntitasRedux.Core.Generator.Tools;

namespace EntitasRedux.Core.Generator
{
	internal sealed class BlueprintClassGenerator : ICodeGenerator
	{
		// Code-generation format strings
		private const string BLUEPRINT_BEHAVIOUR_CLASS_TEMPLATE = @"
using JCMG.EntitasRedux.Blueprints;
using UnityEngine;

/// <summary>
/// Represents a group of <see cref=""JCMG.EntitasRedux.IComponent""/> instances that can be copied to one or more
/// <see cref=""${ContextName}Entity""/>.
/// </summary>
[AddComponentMenu(""EntitasRedux/${ContextName}/${ContextName}BlueprintBehaviour"")]
public partial class ${ContextName}BlueprintBehaviour : BlueprintBehaviourBase, I${ContextName}Blueprint
{
	/// <summary>
	/// Applies all components in the blueprint to <paramref name=""entity""/>.
	/// </summary>
	/// <param name=""entity""></param>
	public void ApplyToEntity(${ContextName}Entity entity)
	{
		for (var i = 0; i < _components.Count; i++)
		{
			var component = _components[i];
			if(_components[i] == null)
			{
				continue;
			}

			var index = ${ContextName}ComponentsLookup.GetComponentIndex(component);
			if (index != -1)
			{
				entity.CopyComponentTo(component);
			}
			else
			{
				Debug.LogWarningFormat(
					JCMG.EntitasRedux.RuntimeConstants.COMPONENT_INDEX_DOES_NOT_EXIST_FOR_TYPE_FORMAT,
					component.GetType(),
					typeof(${ContextName}ComponentsLookup));
			}
		}
	}

	protected override void OnValidate()
	{
		base.OnValidate();

		// Remove any components that no longer belong to this context.
		for (var i = _components.Count - 1; i >= 0; i--)
		{
			var index = ${ContextName}ComponentsLookup.GetComponentIndex(_components[i]);
			if (index == -1)
			{
				_components.RemoveAt(i);
			}
		}
	}
}
";

		private const string BLUEPRINT_CONFIG_CLASS_TEMPLATE = @"
using JCMG.EntitasRedux.Blueprints;
using UnityEngine;

/// <summary>
/// Represents a group of <see cref=""JCMG.EntitasRedux.IComponent""/> instances that can be copied to one or more
/// <see cref=""${ContextName}Entity""/>.
/// </summary>
[CreateAssetMenu(fileName = ""New${ContextName}Blueprint"", menuName = ""EntitasRedux/${ContextName}/${ContextName}Blueprint"")]
public partial class ${ContextName}Blueprint : BlueprintBase, I${ContextName}Blueprint
{
	/// <summary>
	/// Applies all components in the blueprint to <paramref name=""entity""/>.
	/// </summary>
	/// <param name=""entity""></param>
	public void ApplyToEntity(${ContextName}Entity entity)
	{
		for (var i = 0; i < _components.Count; i++)
		{
			var component = _components[i];
			if(_components[i] == null)
			{
				continue;
			}

			var index = ${ContextName}ComponentsLookup.GetComponentIndex(component);
			if (index != -1)
			{
				entity.CopyComponentTo(component);
			}
			else
			{
				Debug.LogWarningFormat(
					JCMG.EntitasRedux.RuntimeConstants.COMPONENT_INDEX_DOES_NOT_EXIST_FOR_TYPE_FORMAT,
					component.GetType(),
					typeof(${ContextName}ComponentsLookup));
			}
		}
	}

	protected override void OnValidate()
	{
		base.OnValidate();

		// Remove any components that no longer belong to this context.
		for (var i = _components.Count - 1; i >= 0; i--)
		{
			var index = ${ContextName}ComponentsLookup.GetComponentIndex(_components[i]);
			if (index == -1)
			{
				_components.RemoveAt(i);
			}
		}
	}
}
";

		private const string BLUEPRINT_INTERFACE_TEMPLATE = @"
/// <summary>
/// Represents a group of <see cref=""JCMG.EntitasRedux.IComponent""/> instances that can be copied to one or more
/// <see cref=""${ContextName}Entity""/>.
/// </summary>
public interface I${ContextName}Blueprint
{
	/// <summary>
	/// Applies all components in the blueprint to <paramref name=""entity""/>.
	/// </summary>
	/// <param name=""entity""></param>
	void ApplyToEntity(${ContextName}Entity entity);
}
";

		// Find and replace tokens
		private const string CONTEXT_NAME_TOKEN = "${ContextName}";

		private const string BLUEPRINT_BEHAVIOUR_FILENAME_FORMAT = "${ContextName}BlueprintBehaviour";
		private const string BLUEPRINT_CONFIG_FILENAME_FORMAT = "${ContextName}Blueprint";
		private const string BLUEPRINT_INTERFACE_FILENAME_FORMAT = "I${ContextName}Blueprint";

		public CodeGenFile[] Generate(IEnumerable<CodeGeneratorData> data)
		{
			var codeGenFilesResult = new List<CodeGenFile>();

			// Create a blueprint class per context
			var contextNames = data.OfType<ComponentData>()
				.SelectMany(s => s.GetContextNames())
				.Distinct()
				.ToArray();

			for (var i = 0; i < contextNames.Length; i++)
			{
				var contextName = contextNames[i];

				// Create Blueprint Interface
				var interfaceFilename = BLUEPRINT_INTERFACE_FILENAME_FORMAT.Replace(CONTEXT_NAME_TOKEN, contextName);
				var interfaceFileContents = BLUEPRINT_INTERFACE_TEMPLATE.Replace(CONTEXT_NAME_TOKEN, contextName);
				codeGenFilesResult.Add(new CodeGenFile(interfaceFilename.AddSourceExtension(), interfaceFileContents));

				// Create Blueprint ScriptableObject
				var blueprintFilename = BLUEPRINT_CONFIG_FILENAME_FORMAT.Replace(CONTEXT_NAME_TOKEN, contextName);
				var blueprintFileContents = BLUEPRINT_CONFIG_CLASS_TEMPLATE.Replace(CONTEXT_NAME_TOKEN, contextName);
				codeGenFilesResult.Add(new CodeGenFile(blueprintFilename.AddSourceExtension(), blueprintFileContents));

				// Create Blueprint MonoBehaviour
				var behaviourFilename = BLUEPRINT_BEHAVIOUR_FILENAME_FORMAT.Replace(CONTEXT_NAME_TOKEN, contextName);
				var behaviourFileContents = BLUEPRINT_BEHAVIOUR_CLASS_TEMPLATE.Replace(CONTEXT_NAME_TOKEN, contextName);
				codeGenFilesResult.Add(new CodeGenFile(behaviourFilename.AddSourceExtension(), behaviourFileContents));
			}

			return codeGenFilesResult.ToArray();
		}
	}
}