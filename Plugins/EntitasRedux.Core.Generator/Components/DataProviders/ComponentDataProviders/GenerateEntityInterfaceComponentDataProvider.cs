using System.Linq;
using EntitasRedux.Core.Generator.Generator;

namespace EntitasRedux.Core.Generator
{
	internal sealed class GenerateEntityInterfaceComponentDataProvider : IComponentDataProvider
	{
		public void Provide(ICachedNamedTypeSymbol cachedNamedTypeSymbol, ComponentData data)
		{
			var attrs = cachedNamedTypeSymbol.GetAttributes(CoreTypes.GenerateEntityInterfaceAttribute);
			data.IsGenerateEntityInterface(attrs.Any() || data.GetContextNames().Length > 1);
		}
	}
}