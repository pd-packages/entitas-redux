using EntitasRedux.Core.Generator.Generator;

namespace EntitasRedux.Core.Generator
{
	internal sealed class IsListenerComponentDataProvider : IComponentDataProvider
	{
		public void Provide(ICachedNamedTypeSymbol cachedNamedTypeSymbol, ComponentData data)
			=> data.IsListener(false);
	}
}
