﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Generator.Generator;

namespace EntitasRedux.Core.Generator
{
	internal sealed class ComponentDataProvider : IDataProvider
	{
		private static readonly IComponentDataProvider[] DataProviders =
		{
			new ComponentTypeComponentDataProvider(),
			new MemberDataComponentDataProvider(),
			new ContextsComponentDataProvider(),
			new IsUniqueComponentDataProvider(),
			new IsListenerComponentDataProvider(),
			new GenerateEntityInterfaceComponentDataProvider(),
			new FlagPrefixComponentDataProvider(),
			new ShouldGenerateComponentComponentDataProvider(),
			new ShouldGenerateMethodsComponentDataProvider(),
			new ShouldGenerateComponentIndexComponentDataProvider(),
			new EventComponentDataProvider(),
			new CleanupComponentDataProvider()
		};

		public CodeGeneratorData[] GetData(IEnumerable<ICachedNamedTypeSymbol> namedTypeSymbols)
		{
			var generateNamedTypeSymbols = namedTypeSymbols
				.Where(namedTypeSymbol => !namedTypeSymbol.HasAttribute(CoreTypes.DontGenerateAttribute));

			var dataFromComponents = generateNamedTypeSymbols
				.Where(cachedNamedTypeSymbol => cachedNamedTypeSymbol.ImplementsInterface(CoreTypes.IComponent))
				.Where(cachedNamedTypeSymbol => !cachedNamedTypeSymbol.NamedTypeSymbol.IsAbstract)
				.SelectMany(CreateDataForComponents)
				.ToArray();

			var dataFromNonComponents = generateNamedTypeSymbols
				.Where(cachedNamedTypeSymbol => !cachedNamedTypeSymbol.ImplementsInterface(CoreTypes.IComponent))
				.Where(cachedNamedTypeSymbol => !cachedNamedTypeSymbol.NamedTypeSymbol.IsGenericType)
				.Where(HasContexts)
				.SelectMany(CreateDataForNonComponents)
				.ToArray();

			var mergedData = Merge(dataFromNonComponents, dataFromComponents);
			var dataFromEvents = mergedData
				.Where(data => data.IsEvent())
				.SelectMany(CreateDataForEvents)
				.ToArray();

			return Merge(dataFromEvents, mergedData).ToArray();
		}

		private static ComponentData[] Merge(ComponentData[] priorData, ComponentData[] redundantData)
		{
			return redundantData
				.Concat(priorData)
				.Distinct(new ComponentDataEqualityComparer())
				.ToArray();
		}

		private static ComponentData[] CreateDataForComponents(ICachedNamedTypeSymbol cachedNamedTypeSymbol)
		{
			return GetComponentNames(cachedNamedTypeSymbol)
				.Select(_ =>
				{
					var data = CreateDataForComponent(cachedNamedTypeSymbol);
					return data;
				})
				.ToArray();
		}

		private static ComponentData[] CreateDataForNonComponents(ICachedNamedTypeSymbol cachedNamedTypeSymbol)
		{
			return GetComponentNames(cachedNamedTypeSymbol)
				.Select(
					componentName =>
					{
						var data = CreateDataForComponent(cachedNamedTypeSymbol);
						data.SetTypeName(componentName.AddComponentSuffix());
						data.SetMemberData(new[]
						{
							new MemberData(cachedNamedTypeSymbol, "value")
						});

						return data;
					})
				.ToArray();
		}

		private static ComponentData CreateDataForComponent(ICachedNamedTypeSymbol namedTypeSymbol)
		{
			var data = new ComponentData();
			foreach (var provider in DataProviders)
			{
				provider.Provide(namedTypeSymbol, data);
			}

			return data;
		}

		private static ComponentData[] CreateDataForEvents(ComponentData data)
		{
			return data.GetContextNames()
				.SelectMany(
					contextName =>
						data.GetEventData()
							.Select(
								eventData =>
								{
									var dataForEvent = new ComponentData(data);
									dataForEvent.IsEvent(false);
									dataForEvent.IsUnique(false);
									dataForEvent.IsListener(true);
									dataForEvent.ShouldGenerateComponent(false);
									dataForEvent.RemoveCleanupData();
									var eventComponentName = data.EventComponentName(eventData);
									var eventTypeSuffix = eventData.GetEventTypeSuffix();
									var optionalContextName =
										dataForEvent.GetContextNames().Length > 1 ? contextName : string.Empty;
									var listenerComponentName =
										optionalContextName + eventComponentName + eventTypeSuffix.AddListenerSuffix();
									dataForEvent.SetTypeName(listenerComponentName.AddComponentSuffix());
									dataForEvent.SetMemberData(Array.Empty<MemberData>());
									dataForEvent.SetContextNames(
										new[]
										{
											contextName
										});
									return dataForEvent;
								})
							.ToArray())
				.ToArray();
		}

		private static bool HasContexts(ICachedNamedTypeSymbol cachedNamedTypeSymbol)
		{
			return cachedNamedTypeSymbol.NamedTypeSymbol.GetContextNames().Length != 0;
		}

		private static string[] GetComponentNames(ICachedNamedTypeSymbol namedTypeSymbol) => new[]
		{
			namedTypeSymbol.TypeName
		};
	}
}