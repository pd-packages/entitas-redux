﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Generator.Generator;
using EntitasRedux.Core.Generator.Tools;

namespace EntitasRedux.Core.Generator
{
	public class EventEntityApiGenerator : AbstractGenerator
	{
		private const string TEMPLATE =
			@"public partial class ${EntityType}
{
	public System.IDisposable Subscribe${Event}(${DelegateEventName} value, bool invokeOnSubscribe = ${invokeOnSubscribe})
	{
		var componentListener = Has${EventListener}
			? ${eventListener}
			: CreateComponent<${EventListenerComponent}>(${componentListenerIndex});
		componentListener.Delegate += value;
		ReplaceComponent(${componentListenerIndex}, componentListener);
		${invokeEvent}

		return new JCMG.EntitasRedux.Events.EventDisposable<${DelegateEventName}>(CreationIndex, value, Unsubscribe${Event});
	}

	private void Unsubscribe${Event}(int creationIndex, ${DelegateEventName} value)
	{
		if(!IsEnabled || CreationIndex != creationIndex)
			return;

		var index = ${componentListenerIndex};
		var component = ${eventListener};
		component.Delegate -= value;
		if (${eventListener}.IsEmpty)
		{
			RemoveComponent(index);
		}
		else
		{
			ReplaceComponent(index, component);
		}
	}
}
";

		private const string INVOKE_EVENT_TEMPLATE =
			@"if(invokeOnSubscribe && ${checkComponent})
		{
${delegateInvoke}
		}";

		public override CodeGenFile[] Generate(IEnumerable<CodeGeneratorData> data)
		{
			return data
				.OfType<ComponentData>()
				.Where(d => d.IsEvent())
				.SelectMany(Generate)
				.ToArray();
		}

		private CodeGenFile[] Generate(ComponentData data)
		{
			return data.GetContextNames()
				.SelectMany(contextName => Generate(contextName, data))
				.ToArray();
		}

		private CodeGenFile[] Generate(string contextName, ComponentData data)
		{
			return data.GetEventData()
				.Select(
					eventData =>
					{
						var fileContent = TEMPLATE
							.Replace("${componentListenerIndex}", data.ComponentListenerIndex(contextName, eventData))
							.Replace("${invokeEvent}", GetInvokeEvent(data, eventData))
							.Replace("${invokeOnSubscribe}", eventData.eventType == EventType.Added ? "true" : "false")
							.Replace(data, contextName, eventData);

						return new CodeGenFile(
							data.EventListenerFileName(contextName, eventData)
								.AddComponentSuffix()
								.AddSourceExtension(),
							fileContent
						);
					})
				.ToArray();
		}

		private string GetInvokeEvent(ComponentData data, EventData eventData)
		{
			var checkComponent = eventData.eventType == EventType.Added
				? "HasComponent(${Index})"
				: "!HasComponent(${Index})";

			var componentCache = data.IsFlag() || eventData.eventType == EventType.Removed
				? string.Empty
				: $"			var component = {data.ComponentNameValidUppercaseFirst()};\n";

			var memberData = data.GetMemberData();
			var methodArgs = memberData.Length == 0 || eventData.eventType == EventType.Removed
				? string.Empty
				: ", " + string.Join(", ", memberData.Select(i => "component." + i.name));

			var delegateInvoke = componentCache + $"			value(this{methodArgs});";

			return INVOKE_EVENT_TEMPLATE
				.Replace("${checkComponent}", checkComponent)
				.Replace("${delegateInvoke}", delegateInvoke);
		}
	}
}