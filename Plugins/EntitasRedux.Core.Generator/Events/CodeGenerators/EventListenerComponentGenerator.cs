﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Linq;
using EntitasRedux.Core.Generator.Generator;
using EntitasRedux.Core.Generator.Tools;

namespace EntitasRedux.Core.Generator
{
	internal sealed class EventListenerComponentGenerator : AbstractGenerator
	{
		private const string TEMPLATE =
			@"[JCMG.EntitasRedux.DontGenerate(false)]
public sealed class ${EventListenerComponent} : JCMG.EntitasRedux.IComponent, System.IDisposable
{
	public event ${DelegateEventName} Delegate;

	public bool IsEmpty => Delegate == null;

	public void Invoke(${ContextName}Entity entity${methodParameters}) => Delegate(entity${methodArgs});

	void System.IDisposable.Dispose() => Delegate = null;
}
";

		public override CodeGenFile[] Generate(IEnumerable<CodeGeneratorData> data)
		{
			return data
				.OfType<ComponentData>()
				.Where(d => d.IsEvent())
				.SelectMany(Generate)
				.ToArray();
		}

		private CodeGenFile[] Generate(ComponentData data)
		{
			return data.GetContextNames()
				.SelectMany(contextName => Generate(contextName, data))
				.ToArray();
		}

		private CodeGenFile[] Generate(string contextName, ComponentData data)
		{
			return data.GetEventData()
				.Select(eventData =>
				{
					var methodParameters = ", " + data.GetMemberData().GetMethodParameters(false);
					var methodArgs =
						", " + string.Join(", ", data.GetMemberData().Select(i => i.name.LowercaseFirst()));

					if (data.IsFlag() || eventData.eventType == EventType.Removed)
					{
						methodParameters = string.Empty;
						methodArgs = string.Empty;
					}

					var fileContent = TEMPLATE
						.Replace("${methodParameters}", methodParameters)
						.Replace("${methodArgs}", methodArgs)
						.Replace(data, contextName, eventData);

					return new CodeGenFile(
						data.EventListenerFileName(contextName, eventData).AddComponentSuffix().AddSourceExtension(),
						fileContent
					);
				})
				.ToArray();
		}
	}
}