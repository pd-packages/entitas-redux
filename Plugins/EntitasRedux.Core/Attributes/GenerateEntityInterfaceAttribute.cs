using System;

namespace JCMG.EntitasRedux
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class GenerateEntityInterfaceAttribute : Attribute
	{
	}
}