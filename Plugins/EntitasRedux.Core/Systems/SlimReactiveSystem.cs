using System;
using System.Runtime.CompilerServices;

namespace JCMG.EntitasRedux
{
	public abstract class UpdateSlimReactiveSystem<TEntity> : SlimReactiveSystem<TEntity>, IUpdateSystem
		where TEntity : class, IEntity
	{
		protected UpdateSlimReactiveSystem(IContext<TEntity> context) : base(context)
		{
		}

		public void Update() => Execute();
	}

	public abstract class LateUpdateSlimReactiveSystem<TEntity> : SlimReactiveSystem<TEntity>, ILateUpdateSystem
		where TEntity : class, IEntity
	{
		protected LateUpdateSlimReactiveSystem(IContext<TEntity> context) : base(context)
		{
		}

		public void LateUpdate() => Execute();
	}

	public abstract class CleanupSlimReactiveSystem<TEntity> : SlimReactiveSystem<TEntity>, ICleanupSystem
		where TEntity : class, IEntity
	{
		protected CleanupSlimReactiveSystem(IContext<TEntity> context) : base(context)
		{
		}

		public void Cleanup() => Execute();
	}

	public abstract class SlimReactiveSystem<TEntity> : IReactiveSystem
		where TEntity : class, IEntity
	{
		private readonly SlimCollector<TEntity> _collector;
		private readonly Func<TEntity, bool> _filterCached;

		private string _toStringCache;

		protected SlimReactiveSystem(IContext<TEntity> context)
		{
			_filterCached = Filter;
			_collector = new SlimCollector<TEntity>(context, ComponentIndex, EventType);
		}

		protected abstract int ComponentIndex { get; }

		protected abstract EventType EventType { get; }

		void IReactiveSystem.Activate() => _collector.Activate();

		void IReactiveSystem.Deactivate() => _collector.Deactivate();

		void IReactiveSystem.Clear() => _collector.ClearCollectedEntities();

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		protected void Execute()
		{
			if (_collector.Count == 0)
				return;

			using var _ = _collector.GetEntities(_filterCached, out var buffer);
			_collector.ClearCollectedEntities();
			if (buffer.Length == 0)
				return;

			Execute(buffer);
		}

		protected abstract bool Filter(TEntity entity);

		protected abstract void Execute(ReadOnlySpan<TEntity> entities);

		public override string ToString() => _toStringCache ??= "SlimReactiveSystem(" + GetType().Name + ")";
	}
}