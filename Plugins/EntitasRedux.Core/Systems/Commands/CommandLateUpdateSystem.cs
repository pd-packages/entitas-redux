namespace JCMG.EntitasRedux.Commands
{
	public abstract class CommandLateUpdateSystem<TCommand> : CommandSystem<TCommand>, ILateUpdateSystem
		where TCommand : struct
	{
		protected CommandLateUpdateSystem(ICommandBuffer commandBuffer) : base(commandBuffer)
		{
		}

		public void LateUpdate() => ExecuteCommands();
	}
}