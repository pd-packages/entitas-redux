using System;

namespace JCMG.EntitasRedux
{
	public abstract class FixedReactiveSystem<TEntity> : IReactiveSystem, IFixedUpdateSystem
		where TEntity : class, IEntity
	{
		private readonly ICollector<TEntity> _collector;
		private readonly Func<TEntity, bool> _filterCached;

		private string _toStringCache;

		protected FixedReactiveSystem(IContext<TEntity> context)
		{
			_collector = GetTrigger(context);
			_filterCached = Filter;
		}

		protected FixedReactiveSystem(ICollector<TEntity> collector)
		{
			_collector = collector;
			_filterCached = Filter;
		}

		protected abstract ICollector<TEntity> GetTrigger(IContext<TEntity> context);

		protected abstract bool Filter(TEntity entity);

		protected abstract void Execute(ReadOnlySpan<TEntity> entities);

		public void Activate()
		{
			_collector.Activate();
		}

		public void Deactivate()
		{
			_collector.Deactivate();
		}

		public void Clear()
		{
			_collector.ClearCollectedEntities();
		}

		public void FixedUpdate()
		{
			var count = _collector.Count;
			if (count == 0)
				return;

			using var _ = _collector.GetEntities(_filterCached, out var buffer);
			_collector.ClearCollectedEntities();

			if (buffer.Length == 0)
				return;

			Execute(buffer);
		}

		public override string ToString() => _toStringCache ??= "ReactiveSystem(" + GetType().Name + ")";

		~FixedReactiveSystem()
		{
			Deactivate();
		}
	}
}