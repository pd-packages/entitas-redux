using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Native
{
	internal class NoAllocUtils
	{
		private delegate Array Internal_ExtractArrayFromList(object list);

		private delegate void Internal_ResizeList(object list, int size);


		private static readonly Internal_ExtractArrayFromList ExtractArrayFromListDelegate;
		private static readonly Internal_ResizeList ResizeListDelegate;

		static NoAllocUtils()
		{
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			var assembly = assemblies.First(f
				=> f.FullName == "UnityEngine.CoreModule, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
			var noAllocType = assembly.GetType("UnityEngine.NoAllocHelpers");

			var extractArrayFromListMethodInfo = noAllocType.GetMethod("ExtractArrayFromList");
			ExtractArrayFromListDelegate =
				extractArrayFromListMethodInfo.CreateDelegate(typeof(Internal_ExtractArrayFromList)) as
					Internal_ExtractArrayFromList;

			var internalResizeListMethodInfo =
				noAllocType.GetMethod("Internal_ResizeList", BindingFlags.Static | BindingFlags.NonPublic);
			ResizeListDelegate =
				internalResizeListMethodInfo.CreateDelegate(typeof(Internal_ResizeList)) as Internal_ResizeList;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T[] ExtractArrayFromList<T>(List<T> list)
			=> (T[])ExtractArrayFromListDelegate(list);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void ResizeList<T>(List<T> list, int size)
		{
			if (list.Capacity < size)
				list.Capacity = size;

			ResizeListDelegate(list, size);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void ResizeListUnsafe<T>(List<T> list, int size) => ResizeListDelegate(list, size);
	}
}