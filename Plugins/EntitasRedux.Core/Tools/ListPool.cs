using System;
using System.Collections.Generic;

namespace JCMG.EntitasRedux
{
	public class ListPool<T>
	{
		private static readonly ListPool<T> Instance = new();

		private readonly Stack<List<T>> _pool = new();
		private readonly Action<List<T>> _releaseCached;

		private ListPool()
		{
			_releaseCached = Release;
		}

		public static PooledObject<List<T>> Get(out List<T> list)
		{
			list = Instance._pool.Count > 0 ? Instance._pool.Pop() : new List<T>();
			return new PooledObject<List<T>>(list, Instance._releaseCached);
		}

		private void Release(List<T> list)
		{
			list.Clear();
			_pool.Push(list);
		}
	}

	public struct PooledObject<T> : IDisposable
	{
		private readonly T _obj;
		private readonly Action<T> _release;

		public PooledObject(T obj, Action<T> release)
		{
			_obj = obj;
			_release = release;
		}

		public void Dispose() => _release(_obj);
	}
}