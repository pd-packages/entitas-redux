using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using EntitasRedux.Core.Libs;
using EntitasRedux.Core.Libs.Collections;

namespace JCMG.EntitasRedux
{
	public struct EntityRepo
	{
		public const int INITIAL_SIZE = 32;
	}
	
	public struct EntityRepo<TEntity>
		where TEntity : class, IEntity
	{
		public readonly Func<GenId, TEntity> CreateEntity;

		public IntStack Reusable;
		public TEntity[] Pool;
		public int Length;
		public int ActiveCount;
		public int CreationIndex;

		public EntityRepo(int startCreationIndex, Func<GenId, TEntity> createEntity) : this()
		{
			CreateEntity = createEntity;
			CreationIndex = startCreationIndex;
			Reusable = IntStack.Create(EntityRepo.INITIAL_SIZE);
			Pool = new TEntity[EntityRepo.INITIAL_SIZE];
		}
	}

	public static class EntityRepoExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Alloc<TEntity>(this ref EntityRepo<TEntity> repo, out TEntity entity)
			where TEntity : class, IEntity
		{
			repo.ActiveCount++;

			if (repo.Reusable.Count > 0)
			{
				var index = repo.Reusable.Pop();
				entity = repo.Pool[index];
				entity.Reactivate(repo.CreationIndex++);
				return false;
			}

			if (repo.Length >= repo.Pool.Length)
				Array.Resize(ref repo.Pool, repo.Pool.Length << 1);

			entity = repo.CreateEntity(new GenId(repo.Length));
			entity.Reactivate(repo.CreationIndex++);
			repo.Pool[repo.Length++] = entity;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Has<TEntity>(this ref EntityRepo<TEntity> repo, GenId id)
			where TEntity : class, IEntity
		{
			if (repo.Reusable.Has(id.Index))
				return false;

			var entity = repo.Pool[id.Index];
			return entity.Id.CreationIndex == id.CreationIndex;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool Release<TEntity>(this ref EntityRepo<TEntity> repo, GenId id)
			where TEntity : class, IEntity
		{
			if (repo.Reusable.Has(id.Index))
				return false;

			var entity = repo.Pool[id.Index];
			if (entity.Id.CreationIndex != id.CreationIndex)
				return false;

			repo.Reusable.Push(id.Index);
			repo.ActiveCount--;
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void ReleaseAll<TEntity>(this ref EntityRepo<TEntity> repo)
			where TEntity : class, IEntity
		{
			for (var i = 0; i < repo.Length; i++)
			{
				if (repo.Reusable.Has(i))
					continue;

				var entity = repo.Pool[i];
				entity.Destroy();
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TEntity GetById<TEntity>(this ref EntityRepo<TEntity> repo, GenId id)
			where TEntity : class, IEntity
		{
			var entity = repo.Pool[id.Index];
			if (entity.Id.CreationIndex != id.CreationIndex || !entity.IsEnabled)
				return null;

			return entity;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void GroupHandleEntitySilently<TEntity>(
			this ref EntityRepo<TEntity> repo,
			IGroup<TEntity> collection
		)
			where TEntity : class, IEntity
		{
			for (var i = 0; i < repo.Length; i++)
			{
				if (repo.Reusable.Has(i))
					continue;

				collection.HandleEntitySilently(repo.Pool[i]);
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void GetEntities<TEntity>(
			this ref EntityRepo<TEntity> repo,
			ICollection<TEntity> collection
		)
			where TEntity : class, IEntity
		{
			for (var i = 0; i < repo.Length; i++)
			{
				if (repo.Reusable.Has(i))
					continue;

				collection.Add(repo.Pool[i]);
			}
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void ResetCreationIndex<TEntity>(this ref EntityRepo<TEntity> repo)
			where TEntity : class, IEntity
		{
			repo.CreationIndex = 0;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TEntity[] ToArray<TEntity>(this ref EntityRepo<TEntity> repo)
			where TEntity : class, IEntity
		{
			var entities = new TEntity[repo.ActiveCount];
			var index = 0;

			for (var i = 0; i < repo.Length; i++)
			{
				if (repo.Reusable.Has(i))
					continue;

				entities[index++] = repo.Pool[i];
			}

			return entities;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<TEntity> AsReadOnlySpan<TEntity>(this ref EntityRepo<TEntity> repo)
			where TEntity : class, IEntity
			=> new(repo.Pool, 0, repo.Length);
	}
}