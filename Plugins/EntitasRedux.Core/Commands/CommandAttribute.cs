using System;

namespace JCMG.EntitasRedux.Commands
{
	[AttributeUsage(AttributeTargets.Struct)]
	public class CommandAttribute : Attribute
	{
	}
}