using System;
using System.Collections.Generic;
using EntitasRedux.Core.Libs.Collections;
using Native;

namespace JCMG.EntitasRedux
{
	public class SlimCollector<TEntity> : ICollector<TEntity>
		where TEntity : class, IEntity
	{
		private readonly IContext<TEntity> _context;
		private readonly EventType _eventType;
		private readonly int _componentIndex;
		private readonly EntityComponentChanged _addedComponentCached;
		private readonly EntityComponentChanged _removedComponentCached;
		private readonly EntityComponentReplaced _replacedComponentCached;
		private readonly EntityEvent _destroyEntityCached;

		private IntSet _entitiesIdx = IntSet.Create();

		public SlimCollector(
			IContext<TEntity> context,
			int componentIndex,
			EventType eventType = EventType.Added
		)
		{
			_context = context;
			_componentIndex = componentIndex;
			_eventType = eventType;
			_addedComponentCached = OnComponentAdded;
			_removedComponentCached = OnComponentRemoved;
			_replacedComponentCached = OnComponentReplaced;
			_destroyEntityCached = OnDestroyEntity;

			Activate();
		}

		public void Activate()
		{
			_context.OnEntityCreated += _eventType switch
			{
				EventType.Added => OnEntityCreatedAddEvent,
				EventType.Removed => OnEntityCreatedRemoveEvent,
				_ => throw new ArgumentOutOfRangeException()
			};

			using var _ = ListPool<TEntity>.Get(out var buffer);
			_context.GetEntities(buffer);

			var length = buffer.Count;
			var bufferArray = NoAllocUtils.ExtractArrayFromList(buffer);

			switch (_eventType)
			{
				case EventType.Added:
					for (var i = 0; i < length; i++)
						OnEntityCreatedAddEvent(_context, bufferArray[i]);

					break;
				case EventType.Removed:
					for (var i = 0; i < length; i++)
						OnEntityCreatedRemoveEvent(_context, bufferArray[i]);

					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public void Deactivate()
		{
			_context.OnEntityCreated -= _eventType switch
			{
				EventType.Added => OnEntityCreatedAddEvent,
				EventType.Removed => OnEntityCreatedRemoveEvent,
				_ => throw new ArgumentOutOfRangeException()
			};

			ClearCollectedEntities();
			
			using var _ = ListPool<TEntity>.Get(out var buffer);
			_context.GetEntities(buffer);

			var length = buffer.Count;
			var bufferArray = NoAllocUtils.ExtractArrayFromList(buffer);

			switch (_eventType)
			{
				case EventType.Added:
					for (var i = 0; i < length; i++)
						OnRemoveEntityAddEvent(bufferArray[i]);

					break;
				case EventType.Removed:
					for (var i = 0; i < length; i++)
						OnRemoveEntityRemoveEvent(bufferArray[i]);

					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public int Count => _entitiesIdx.Count();

		public IEnumerable<IEntity> GetEntities()
		{
			var list = new List<TEntity>(_entitiesIdx.Count());
			var rawEntities = _context.RawEntities;
			var indexes = _entitiesIdx.AsSpan();
			for (var i = 0; i < indexes.Length; i++)
			{
				var entity = rawEntities[indexes[i]];
				list.Add(entity);
			}

			return list;
		}

		public PooledObject<List<TEntity>> GetEntities(
			Func<TEntity, bool> filter,
			out Span<TEntity> buffer
		)
		{
			var pooledObject = ListPool<TEntity>.Get(out var list);
			var count = _entitiesIdx.Count();
			if (count > list.Capacity)
				list.Capacity = count;

			var bufferArray = NoAllocUtils.ExtractArrayFromList(list);
			var size = 0;

			var rawEntities = _context.RawEntities;
			var indexes = _entitiesIdx.AsSpan();
			for (var i = 0; i < indexes.Length; i++)
			{
				var entity = rawEntities[indexes[i]];
				if (!filter(entity))
					continue;

				bufferArray[size++] = entity;
			}

			buffer = new Span<TEntity>(bufferArray, 0, size);
			return pooledObject;
		}

		public void ClearCollectedEntities()
		{
			if (_entitiesIdx.Count() == 0)
				return;

			_entitiesIdx.Clear();
		}

		private void OnEntityCreatedAddEvent(IContext context, IEntity entity)
		{
			entity.OnComponentAdded += _addedComponentCached;
			entity.OnComponentReplaced += _replacedComponentCached;
			entity.OnDestroyEntity += _destroyEntityCached;
		}

		private void OnRemoveEntityAddEvent(IEntity entity)
		{
			entity.OnComponentAdded -= _addedComponentCached;
			entity.OnComponentReplaced -= _replacedComponentCached;
			entity.OnDestroyEntity -= _destroyEntityCached;
		}

		private void OnEntityCreatedRemoveEvent(IContext context, IEntity entity)
		{
			entity.OnComponentRemoved += _removedComponentCached;
			entity.OnDestroyEntity += _destroyEntityCached;
		}

		private void OnRemoveEntityRemoveEvent(IEntity entity)
		{
			entity.OnComponentRemoved -= _removedComponentCached;
			entity.OnDestroyEntity -= _destroyEntityCached;
		}

		private void OnDestroyEntity(IEntity entity)
		{
			if (!_entitiesIdx.Remove(entity.Id.Index))
				return;

			switch (_eventType)
			{
				case EventType.Added:
					OnRemoveEntityAddEvent(entity);
					break;
				case EventType.Removed:
					OnRemoveEntityRemoveEvent(entity);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void OnComponentAdded(IEntity entity, int index, IComponent component)
		{
			if (_componentIndex != index)
				return;

			_entitiesIdx.Add(entity.Id.Index);
		}

		private void OnComponentReplaced(
			IEntity entity,
			int index,
			IComponent previousComponent,
			IComponent newComponent
		)
		{
			if (_componentIndex != index)
				return;

			_entitiesIdx.Add(entity.Id.Index);
		}

		private void OnComponentRemoved(IEntity entity, int index, IComponent component)
		{
			if (_componentIndex != index)
				return;

			_entitiesIdx.Add(entity.Id.Index);
		}
	}
}