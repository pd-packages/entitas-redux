﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using EntitasRedux.Core.Libs.Collections;
using Native;

namespace JCMG.EntitasRedux
{
	/// <summary>
	/// A Collector can observe one or more groups from the same context
	/// and collects changed entities based on the specified groupEvent.
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public class Collector<TEntity> : ICollector<TEntity>
		where TEntity : class, IEntity
	{
		private readonly GroupEvent[] _groupEvents;
		private readonly IContext<TEntity> _context;
		private readonly IGroup<TEntity>[] _groups;
		private readonly GroupChanged<TEntity> _addEntityCache;
		private readonly ContextEntityChanged _removeEntityCache;

		private IntSet _entitiesIdx = IntSet.Create();
		private StringBuilder _toStringBuilder;
		private string _toStringCache;

		public Collector(IContext<TEntity> context, IGroup<TEntity> group, GroupEvent groupEvent = GroupEvent.Added)
			: this(context, new[] { group }, new[] { groupEvent })
		{
		}

		/// <summary>
		/// Creates a Collector and will collect changed entities
		/// based on the specified groupEvents.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="groups"></param>
		/// <param name="groupEvents"></param>
		public Collector(IContext<TEntity> context, IGroup<TEntity>[] groups, GroupEvent[] groupEvents)
		{
			_context = context;
			_groups = groups;
			_groupEvents = groupEvents;

			if (groups.Length != groupEvents.Length)
			{
				throw new CollectorException(
					"Unbalanced count with groups (" +
					groups.Length +
					") and group events (" +
					groupEvents.Length +
					").",
					"Group and group events count must be equal.");
			}

			_toStringBuilder = null;
			_toStringCache = null;
			_addEntityCache = null;
			_addEntityCache = AddEntity;
			_removeEntityCache = OnEntityDestroyed;
			Activate();
		}

		private void AddEntity(IGroup<TEntity> group, TEntity entity, int index, IComponent component)
			=> _entitiesIdx.Add(entity.Id.Index);

		private void OnEntityDestroyed(IContext context, IEntity entity)
			=> _entitiesIdx.Remove(entity.Id.Index);

		public override string ToString()
		{
			if (_toStringCache != null)
				return _toStringCache;

			_toStringBuilder ??= new StringBuilder();
			_toStringBuilder.Length = 0;
			_toStringBuilder.Append("Collector(");

			const string separator = ", ";
			var lastSeparator = _groups.Length - 1;
			for (var i = 0; i < _groups.Length; i++)
			{
				_toStringBuilder.Append(_groups[i]);
				if (i < lastSeparator)
				{
					_toStringBuilder.Append(separator);
				}
			}

			_toStringBuilder.Append(")");
			_toStringCache = _toStringBuilder.ToString();

			return _toStringCache;
		}

		public IEnumerable<IEntity> GetEntities()
		{
			var list = new List<TEntity>(_entitiesIdx.Count());
			var entities = _context.RawEntities;
			var indexes = _entitiesIdx.AsSpan();
			for (var i = 0; i < indexes.Length; i++)
			{
				var entity = entities[indexes[i]];
				if (entity == null)
					continue;

				list.Add(entity);
			}

			return list;
		}

		public PooledObject<List<TEntity>> GetEntities(
			Func<TEntity, bool> filter,
			out Span<TEntity> buffer
		)
		{
			var pooledObject = ListPool<TEntity>.Get(out var list);
			var count = _entitiesIdx.Count();
			if (count > list.Capacity)
				list.Capacity = count;

			var bufferArray = NoAllocUtils.ExtractArrayFromList(list);
			var size = 0;

			var rawEntities = _context.RawEntities;
			var indexes = _entitiesIdx.AsSpan();
			for (var i = 0; i < indexes.Length; i++)
			{
				var entity = rawEntities[indexes[i]];
				if (!filter(entity))
					continue;

				bufferArray[size++] = entity;
			}

			buffer = new Span<TEntity>(bufferArray, 0, size);
			return pooledObject;
		}

		~Collector()
		{
			Deactivate();
		}

		/// Returns the number of all collected entities.
		public int Count => _entitiesIdx.Count();

		/// <summary>
		/// Activates the Collector and will start collecting
		/// changed entities. Collectors are activated by default.
		/// </summary>
		public void Activate()
		{
			for (var i = 0; i < _groups.Length; i++)
			{
				var group = _groups[i];
				var groupEvent = _groupEvents[i];
				switch (groupEvent)
				{
					case GroupEvent.Added:
						group.OnEntityAdded -= _addEntityCache;
						group.OnEntityAdded += _addEntityCache;
						break;
					case GroupEvent.Removed:
						group.OnEntityRemoved -= _addEntityCache;
						group.OnEntityRemoved += _addEntityCache;
						break;
					case GroupEvent.AddedOrRemoved:
						group.OnEntityAdded -= _addEntityCache;
						group.OnEntityAdded += _addEntityCache;
						group.OnEntityRemoved -= _addEntityCache;
						group.OnEntityRemoved += _addEntityCache;
						break;
				}
			}

			_context.OnEntityDestroyed += _removeEntityCache;
		}

		/// <summary>
		/// Deactivates the Collector.
		/// This will also clear all collected entities.
		/// Collectors are activated by default.
		/// </summary>
		public void Deactivate()
		{
			_context.OnEntityDestroyed -= _removeEntityCache;

			for (var i = 0; i < _groups.Length; i++)
			{
				var group = _groups[i];
				group.OnEntityAdded -= _addEntityCache;
				group.OnEntityRemoved -= _addEntityCache;
			}

			ClearCollectedEntities();
		}

		/// <summary>
		/// Clears all collected entities.
		/// </summary>
		public void ClearCollectedEntities()
		{
			if (_entitiesIdx.Count() == 0)
				return;

			_entitiesIdx.Clear();
		}
	}
}

// Position
// Rotation

// Position