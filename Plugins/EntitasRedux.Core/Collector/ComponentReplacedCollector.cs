using System;
using System.Collections.Generic;

namespace JCMG.EntitasRedux
{
	public class ComponentReplacedCollector<TEntity, TComponent, TValue> : ICollector
		where TEntity : class, IEntity
		where TComponent : class
	{
		private readonly IContext<TEntity> _context;
		private readonly int _componentIndex;
		private readonly Func<TComponent, TValue> _getter;
		private readonly TValue _defaultValue;

		private readonly Dictionary<int, ValueReplacedEvent<TValue>> _replaceComponentEvents = new();

		public ComponentReplacedCollector(
			IContext<TEntity> context,
			int componentIndex,
			Func<TComponent, TValue> getter,
			TValue defaultValue
		)
		{
			_context = context;
			_componentIndex = componentIndex;
			_getter = getter;
			_defaultValue = defaultValue;

			Activate();
		}

		public void Activate()
		{
			_context.OnEntityCreated += OnEntityCreated;
		}

		public void Deactivate()
		{
			_context.OnEntityCreated -= OnEntityCreated;

			ClearCollectedEntities();
		}

		public void ClearCollectedEntities() => _replaceComponentEvents.Clear();

		public IEnumerable<IEntity> GetEntities()
			=> throw new NotImplementedException("Not implemented exception");

		public int Count => _replaceComponentEvents.Count;

		public Dictionary<int, ValueReplacedEvent<TValue>> GetCollectedEntities()
			=> _replaceComponentEvents;

		private void OnEntityCreated(IContext context, IEntity entity)
		{
			entity.OnComponentAdded += OnComponentAdded;
			entity.OnComponentReplaced += OnComponentReplaced;
			entity.OnComponentRemoved += OnComponentRemoved;
			entity.OnDestroyEntity += OnDestroyEntity;
		}

		private void OnDestroyEntity(IEntity entity)
		{
			if (entity is TEntity castedEntity)
				_replaceComponentEvents.Remove(castedEntity.Id.Index);

			entity.OnComponentAdded -= OnComponentAdded;
			entity.OnComponentReplaced -= OnComponentReplaced;
			entity.OnComponentRemoved -= OnComponentRemoved;
			entity.OnDestroyEntity -= OnDestroyEntity;
		}

		private void OnComponentAdded(IEntity entity, int index, IComponent component)
		{
			if (_componentIndex != index)
				return;

			_replaceComponentEvents[entity.Id.Index] = new ValueReplacedEvent<TValue>(
				_defaultValue,
				_getter(component as TComponent)
			);
		}

		private void OnComponentReplaced(
			IEntity entity,
			int index,
			IComponent previousComponent,
			IComponent newComponent
		)
		{
			if (_componentIndex != index)
				return;

			_replaceComponentEvents[entity.Id.Index] = new ValueReplacedEvent<TValue>(
				_getter(previousComponent as TComponent),
				_getter(newComponent as TComponent)
			);
		}

		private void OnComponentRemoved(IEntity entity, int index, IComponent component)
		{
			if (_componentIndex != index)
				return;

			_replaceComponentEvents[entity.Id.Index] = new ValueReplacedEvent<TValue>(
				_getter(component as TComponent),
				_defaultValue
			);
		}
	}

	public readonly struct ValueReplacedEvent<TValue>
	{
		public readonly TValue Last;
		public readonly TValue Next;

		public ValueReplacedEvent(TValue last, TValue next)
		{
			Last = last;
			Next = next;
		}
	}
}