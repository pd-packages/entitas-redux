using Microsoft.CodeAnalysis;

namespace EntitasRedux.Core.Plugins.Generator.Tests.Extensions
{
	public static class GeneratedTreeExtensions
	{
		public static void AssertFile(
			this IEnumerable<SyntaxTree> syntaxTrees,
			string fileName,
			string fileContent
		)
		{
			var syntaxTree = syntaxTrees.Single(f => f.FilePath.EndsWith(fileName));
			Assert.Equal(fileContent, syntaxTree.GetText().ToString());
		}
	}
}