using EntitasRedux.Core.Generator;
using JCMG.EntitasRedux;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using CleanupMode = JCMG.EntitasRedux.CleanupMode;
using EventTarget = JCMG.EntitasRedux.EventTarget;
using EventType = JCMG.EntitasRedux.EventType;

namespace EntitasRedux.Core.Plugins.Generator.Tests
{
	public class ComponentGeneratorTests
	{
		private const string CONTEXT_NAME = "Test";

		private static readonly string ContextSample =
			$$"""

			  public sealed partial class {{CONTEXT_NAME}}Attribute : {{typeof(ContextAttribute).FullName}}
			  {
			  }

			  """;

		private static readonly string ComponentSample =
			$$"""

			  using {{typeof(IComponent).Namespace}};
			  using {{typeof(EventAttribute).Namespace}};
			  using {{typeof(EventTarget).Namespace}};
			  using {{typeof(EventType).Namespace}};

			  [{{CONTEXT_NAME}}]
			  [{{nameof(EventAttribute)}}({{nameof(EventTarget)}}.{{EventTarget.Self}})]
			  [{{nameof(EventAttribute)}}({{nameof(EventTarget)}}.{{EventTarget.Self}}, {{nameof(EventType)}}.{{EventType.Removed}})]
			  public class IntComponent : {{nameof(IComponent)}}
			  {
			      public int Value;
			  }

			  """;

		private static readonly string ComponentFlagSample =
			$$"""

			  using {{typeof(IComponent).Namespace}};
			  using {{typeof(EventAttribute).Namespace}};
			  using {{typeof(EventTarget).Namespace}};
			  using {{typeof(EventType).Namespace}};

			  [{{CONTEXT_NAME}}]
			  [{{nameof(EventAttribute)}}({{nameof(EventTarget)}}.{{EventTarget.Self}})]
			  [{{nameof(EventAttribute)}}({{nameof(EventTarget)}}.{{EventTarget.Self}}, {{nameof(EventType)}}.{{EventType.Removed}})]
			  public class FlagComponent : {{nameof(IComponent)}}
			  {
			  }

			  """;

		private static readonly string CleanupRemoveComponentSample =
			$$"""

			  using {{typeof(IComponent).Namespace}};
			  using {{typeof(CleanupAttribute).Namespace}};
			  using {{typeof(CleanupMode).Namespace}};

			  [{{CONTEXT_NAME}}]
			  [{{nameof(CleanupAttribute)}}({{nameof(CleanupMode)}}.{{CleanupMode.RemoveComponent}})]
			  public class DitryComponent : {{nameof(IComponent)}}
			  {
			  }

			  """;

		private static readonly string CleanupDestroyEntitySample =
			$$"""

			  using {{typeof(IComponent).Namespace}};
			  using {{typeof(CleanupAttribute).Namespace}};
			  using {{typeof(CleanupMode).Namespace}};

			  [{{CONTEXT_NAME}}]
			  [{{nameof(CleanupAttribute)}}({{nameof(CleanupMode)}}.{{CleanupMode.DestroyEntity}})]
			  public class DestroyedComponent : {{nameof(IComponent)}}
			  {
			  }

			  """;

		private static readonly string ComponentPrimaryEntityIndexSample =
			$$"""

			  using {{typeof(IComponent).Namespace}};
			  using {{typeof(PrimaryEntityIndexAttribute).Namespace}};

			  [{{CONTEXT_NAME}}]
			  public class IdComponent : {{nameof(IComponent)}}
			  {
			      [{{nameof(PrimaryEntityIndexAttribute)}}]
			      public int Value;
			  }

			  """;

		private static readonly string ComponentEntityIndexSample =
			$$"""

			  using {{typeof(IComponent).Namespace}};
			  using {{typeof(EntityIndexAttribute).Namespace}};

			  [{{CONTEXT_NAME}}]
			  public class OwnerComponent : {{nameof(IComponent)}}
			  {
			      [{{nameof(EntityIndexAttribute)}}]
			      public int Value;
			  }

			  """;

		private static readonly string InheritedComponentSample =
			$$"""

			  using {{typeof(IComponent).Namespace}};

			  [{{CONTEXT_NAME}}]
			  public class InheritedComponent : ParentComponent
			  {
			  }

			  [{{CONTEXT_NAME}}]
			  public class ParentComponent : {{nameof(IComponent)}}
			  {
			      public int Value;
			  }

			  """;

		[Fact]
		public void GenerateFiles()
		{
			// Create an instance of the source generator.
			var generator = new IncrementalGenerator();

			// Source generators should be tested using 'GeneratorDriver'.
			var driver = CSharpGeneratorDriver.Create(generator);

			var referencePaths = new[]
			{
				typeof(object).Assembly.Location, // mscorlib.dll or System.Private.CoreLib.dll
				Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Runtime.dll"),
				Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Console.dll"),
				Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "netstandard.dll"),
				typeof(EventAttribute).Assembly.Location,
			};

			// We need to create a compilation with the required source code.
			var compilation = CSharpCompilation.Create(nameof(ComponentGeneratorTests),
				new[]
				{
					CSharpSyntaxTree.ParseText(ContextSample),
					CSharpSyntaxTree.ParseText(ComponentSample),
					CSharpSyntaxTree.ParseText(ComponentFlagSample),
					CSharpSyntaxTree.ParseText(InheritedComponentSample),
					CSharpSyntaxTree.ParseText(CleanupRemoveComponentSample),
					CSharpSyntaxTree.ParseText(CleanupDestroyEntitySample),
					CSharpSyntaxTree.ParseText(ComponentPrimaryEntityIndexSample),
					CSharpSyntaxTree.ParseText(ComponentEntityIndexSample),
				},
				referencePaths.Select(path => MetadataReference.CreateFromFile(path))
			);

			// Run generators and retrieve all results.
			var runResult = driver.RunGenerators(compilation).GetRunResult();
			var generatedTrees = runResult.GeneratedTrees;

			foreach (var result in runResult.Results)
				Assert.Null(result.Exception);

			// var contextData = new ContextData();
			// contextData.SetContextName(CONTEXT_NAME);
			// var contextsData = new[] { contextData };
			//
			// generatedTrees.AssertFile(
			// 	ContextAttributeGenerator.GetFileName(CONTEXT_NAME),
			// 	ContextAttributeGenerator.Generate(CONTEXT_NAME)
			// );
			//
			// generatedTrees.AssertFile(
			// 	ContextGenerator.GetFileName(CONTEXT_NAME),
			// 	ContextGenerator.Generate(CONTEXT_NAME)
			// );
			//
			// generatedTrees.AssertFile(
			// 	ContextMatcherGenerator.GetFileName(CONTEXT_NAME),
			// 	ContextMatcherGenerator.Generate(CONTEXT_NAME)
			// );
			//
			// generatedTrees.AssertFile(
			// 	ContextsGenerator.GetFileName(),
			// 	ContextsGenerator.Generate(new[] { CONTEXT_NAME })
			// );
			//
			// generatedTrees.AssertFile(
			// 	EntityGenerator.GetFileName(CONTEXT_NAME),
			// 	EntityGenerator.Generate(CONTEXT_NAME)
			// );
		}
	}
}