using JCMG.EntitasRedux.Commands;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace EntitasRedux.Command.Generator.Test;

public class CommandGeneratorTest
{
	private static readonly string Sample1 =
		$$"""

		  [{{typeof(CommandAttribute).Namespace + "." + nameof(CommandAttribute)}}]
		  public struct Sample1Cmd
		  {
		      public int Value;
		  }

		  """;

	private static readonly string Sample2 =
		$$"""

		  using {{typeof(CommandAttribute).Namespace}};

		  [Command]
		  public struct Sample2Cmd
		  {
		      public int Value1;
		      public int Value2;
		  }

		  """;

	private static readonly string Sample3 =
		$$"""

		  using {{typeof(CommandAttribute).Namespace}};

		  namespace Project.Name.Space
		  {

		  [{{nameof(CommandAttribute)}}]
		  public struct Sample3Cmd
		  {
		      public int Value;
		  }

		  }

		  """;

	private static readonly string Sample4 =
		$$"""

		  [{{typeof(CommandAttribute).Namespace + "." + nameof(CommandAttribute)}}]
		  public struct Sample4Cmd
		  {
		  }

		  """;

	private static readonly string Sample5 =
		$$"""

		  using System;

		  [{{typeof(CommandAttribute).Namespace + "." + nameof(CommandAttribute)}}]
		  public struct Sample5Cmd
		  {
		      public Action<int> Callback;
		  }

		  """;

	[Fact]
	public void Generate()
	{
		// Create an instance of the source generator.
		var generator = new CommandExtensionGenerator();

		// Source generators should be tested using 'GeneratorDriver'.
		var driver = CSharpGeneratorDriver.Create(generator);

		var referencePaths = new[]
		{
			typeof(CommandAttribute).Assembly.Location,
			typeof(object).Assembly.Location, // mscorlib.dll or System.Private.CoreLib.dll
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Runtime.dll"),
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Console.dll"),
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "netstandard.dll"),
		};

		// We need to create a compilation with the required source code.
		var compilation = CSharpCompilation.Create(nameof(CommandGeneratorTest),
			new[]
			{
				CSharpSyntaxTree.ParseText(Sample1),
				CSharpSyntaxTree.ParseText(Sample2),
				CSharpSyntaxTree.ParseText(Sample3),
				CSharpSyntaxTree.ParseText(Sample4),
				CSharpSyntaxTree.ParseText(Sample5),
			},
			referencePaths.Select(path => MetadataReference.CreateFromFile(path))
		);

		// Run generators and retrieve all results.
		var runResult = driver.RunGenerators(compilation).GetRunResult();
		var generatedTrees = runResult.GeneratedTrees;

		foreach (var result in runResult.Results)
			Assert.Null(result.Exception);
	}
}