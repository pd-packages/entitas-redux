# Changelog

All notable changes to this project will be documented in this file.

## [Releases]

---

## [v3.0.13](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.13)

### Fixes

- Activate and deactivate `SlimCollector`

---

## [v3.0.12](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.12)

### Fixes

- Command pool initialization

---

## [v3.0.10](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.10)

### Fixes

- Generate create command method with multiple args in command

---

## [v3.0.9](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.9)

### Added

- Command create code generation

### Fixes

- Dispose `CommandBuffer`

---

## [v3.0.8](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.8)

### Fixes

- Use `CreateComponent<T>(int index)` instead of `CreateComponent(int index, Type type)`

---

## [v3.0.7](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.7)

### Fixes

- Try get `EntityLink` component if null reference in `LinkableView`

---

## [v3.0.6](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.6)

### Changed

- Remove empty HashSet allocation when get entity from index by key

---

## [v3.0.5](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.5)

### Fixed

- Source generator print exception in generated file

---

## [v3.0.4](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.4)

### Fixed

- Process `CancellationToken` in code generator

---

## [v3.0.3](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.3)

### Added

- `TryAdd` and `TryRemove` in component api generation

### Fixed

- Process `CancellationToken` in code generator

---

## [v3.0.2](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.2)

### Fixed

- Generate inherited components

---

## [v3.0.1](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.1)

### Fixed

- Generate context with no components

---

## [v3.0.0](https://gitlab.com/pd-packages/entitas-redux/-/tags/v3.0.0)

### Added

- Incremental code generation using Microsoft.CodeAnalysis.CSharp 4.3.1
- GenerateEntityInterfaceAttribute for generate component Entity interface api if component doesn't have multiple
  context usage

### Changed

- **Supported Unity version 2022.3.x**
- Generate component Entity interface api if component has multiple context usages or has
  GenerateEntityInterfaceAttribute

### Removed

- Genesis dependency
- DontGenerateEntityInterfaceAttribute

---

## [v2.4.3](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.4.2)

### Fixed

- SendMessage cannot be called during Awake, CheckConsistency, or OnValidate

---

## [v2.4.2](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.4.2)

### Fixed

- Checking positions length when resize IntSet

---

## [v2.4.1](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.4.1)

### Changed

- Use custom IntSet in Groups and Collectors for performance improvement

---

## [v2.4.0](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.4.0)

### Changed

- Cast components using `as` keyword
- Do not call internal `HasComponent` on the known component
- Return component on the known component without call `GetComponent` if `ENTITAS_FAST_AND_UNSAFE`
- Improve EntityRepo collection speed

---

## [v2.2.14](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.2.14)

### Changed

- Do not check `HasComponent` when you try `GetComponent` and `ENTITAS_FAST_AND_UNSAFE`

---

## [v2.2.13](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.2.13)

### Removed

- Removed custom IntHashSet and IntDictionaryRef because iteration speed is slow

---

## [v2.2.12](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.2.12)

### Changed

- Update Genesis to 2.3.4

---

## [v2.2.11](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.2.11)

### Changed

- IntHashSet and IntDictionaryRef Enumerator to struct for avoid allocation

---

## [v2.2.10](https://gitlab.com/pd-packages/entitas-redux/-/tags/v2.2.10)

### Fixed

- Update Genesis dependency to 2.3.3

---

## [2.2.9] - 2023-11-03

### Fixed

* Draw GUI in inspector Unity 2021.3.31f

---

## [2.2.8] - 2023-10-22

### Changed

* Do not throw Exception when invoke link entity to view multiple times

---

## [2.2.7] - 2023-10-21

### Changed

* Do not invoke Execute in CommandSystem if commands pool is empty

---

## [2.2.6] - 2023-10-21

### Added

* CommandBuffer systems for LateUpdate

---

## [2.2.5] - 2023-09-09

### Fixed

* Remove pointer from flag in EventDisposable

---

## [2.2.4] - 2023-09-09

### Added

* Disposed flag pointer to EventDisposable for do not process Dispose multiple times

---

## [2.2.3] - 2023-09-05

### Fixed

* Do not invoke component changed event by default if subscribed on remove component

---

## [2.2.2] - 2023-08-26

### Fixed

* SlimCollector collector fixes and tests

---

## [2.2.1] - 2023-08-24

### Added

* Use lightweight Collector version called SlimCollector for invoke entity components changed events

---

## [2.2.0] - 2023-08-21

### Changed

* Change entity index to GenId
* Change previous entity buffer to EntityRepo where Entities stored by GenId
* Change HashSet in Groups to IntHashSet for store entities by index
* Generate Subscription methods for listen entity components change like a RX pattern

### Removed

* Generation Listener interfaces

---

### [2.1.0] - 2021-07-13

### Fixes

* Fixed several bugs with Entity Index code generation where index-related methods and code was not generated.

### Changes

* Sealed attribute types EntityIndex, PrimaryEntityIndex, and CustomEntityIndex to ensure stable behavior for the
  EntityIndex code-generator.

---

### [2.0.1] - 2021-07-05

#### Fixes

* Updated Genesis package dependency min version to v2.3.2 to include several fixes for path issues

---

### [2.0.0] - 2021-06-16

#### Changed

* All code-generation plugins have been extracted from Unity as v2 Genesis .Net Core plugin assemblies (use Roslyn over
  Reflection).

## CL Template

This template should be used for any new releases to document any changes.

---

## [v0.0.0](https://gitlab.com/pd-packages/entitas-redux/-/tags/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.