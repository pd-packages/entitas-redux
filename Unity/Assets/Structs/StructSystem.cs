using System;
using System.Collections.Generic;
using JCMG.EntitasRedux;
using UnityEngine;

namespace Structs
{
	public class StructSystem : ReactiveSystem<VisualDebugEntity>
	{
		public StructSystem(VisualDebugContext context) : base(context)
		{
		}

		protected override ICollector<VisualDebugEntity> GetTrigger(IContext<VisualDebugEntity> context)
			=> context.CreateCollector(VisualDebugMatcher.Color.Added());

		protected override bool Filter(VisualDebugEntity entity)
			=> entity.HasColor;

		protected override void Execute(ReadOnlySpan<VisualDebugEntity> entities)
		{
			foreach (var entity in entities)
			{
				Debug.Log($"Entity {entity.Id}, Color: {entity.Color.color}");
			}
		}
	}
}