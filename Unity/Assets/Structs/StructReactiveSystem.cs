using System;
using System.Collections.Generic;
using JCMG.EntitasRedux;
using UnityEngine;

namespace Structs
{
    public class StructReactiveSystem : ReactiveSystem<VisualDebugEntity>
    {
        public StructReactiveSystem(IContext<VisualDebugEntity> context) : base(context)
        {
        }

        protected override ICollector<VisualDebugEntity> GetTrigger(IContext<VisualDebugEntity> context) =>
            context.CreateCollector(VisualDebugMatcher.Color);

        protected override bool Filter(VisualDebugEntity entity) => entity.HasColor;

        protected override void Execute(ReadOnlySpan<VisualDebugEntity> entities)
        {
            foreach (var entity in entities)
            {
                Debug.Log($"StructReactiveSystem, color: {entity.Color.color}");
            }
        }
    }
}