using JCMG.EntitasRedux;

namespace ExampleContent.VisualDebugging
{
	[VisualDebug]
	[Cleanup(CleanupMode.DestroyEntity)]
	public class DestroyedComponent : IComponent
	{
		public override string ToString() => "IsDestroyed";
	}
}