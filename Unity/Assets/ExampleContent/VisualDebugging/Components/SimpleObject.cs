﻿using System.Collections.Generic;

namespace ExampleContent.VisualDebugging
{
	public class SimpleObject
	{
		public int age;
		public CustomObject customObject;
		public Dictionary<string, string> data;
		public PositionComponent intVector;
		public string name;
		public SimpleObject simpleObject;
	}
}
