using System;
using JCMG.EntitasRedux;
using UnityEngine;

namespace ExampleContent.VisualDebugging
{
	[Serializable]
	[VisualDebug]
	public class PositionComponent : IComponent
	{
		public Vector2Int Value;
	}
}