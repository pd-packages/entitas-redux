using JCMG.EntitasRedux;
using UnityEngine;

namespace ExampleContent.VisualDebugging
{
	public class CreateAiPersonSystem : IUpdateSystem
	{
		private readonly VisualDebugContext _context;

		public CreateAiPersonSystem(Contexts contexts)
		{
			_context = contexts.VisualDebug;
		}

		public void Update()
		{
			var count = Random.Range(1, 9);

			for (var i = 0; i < count; i++)
			{
				var entity = _context.CreateEntity();
				entity.AddPerson("bit", "ai_" + i);
			}
		}
	}
}