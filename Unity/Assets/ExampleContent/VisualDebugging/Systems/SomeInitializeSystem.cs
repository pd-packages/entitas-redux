﻿using JCMG.EntitasRedux;
using UnityEngine;

namespace ExampleContent.VisualDebugging
{
	public class SomeInitializeSystem : IInitializeSystem
	{
		public void Initialize()
		{
			Debug.Log("SomeInitializeSystem");
		}
	}
}
