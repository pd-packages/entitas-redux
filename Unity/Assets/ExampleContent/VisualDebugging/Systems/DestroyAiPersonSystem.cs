using JCMG.EntitasRedux;
using UnityEngine;

namespace ExampleContent.VisualDebugging
{
	public class DestroyAiPersonSystem : IUpdateSystem
	{
		private readonly IGroup<VisualDebugEntity> _group;

		private int _iteration;

		public DestroyAiPersonSystem(Contexts contexts)
		{
			_group = contexts.VisualDebug.GetGroup(VisualDebugMatcher.Person);
			_group.OnEntityAdded += (group, entity, index, component) =>
			{
				Debug.Log($"[{nameof(DestroyAiPersonSystem)}] OnEntityAdded: {entity}");
			};
			_group.OnEntityRemoved += (group, entity, index, component) =>
			{
				Debug.Log($"[{nameof(DestroyAiPersonSystem)}] OnEntityRemoved: {entity}");
			};
		}

		public void Update()
		{
			_iteration++;
			Debug.Log($"[{nameof(DestroyAiPersonSystem)}] Iteration: {_iteration}");
			Debug.Log($"[{nameof(DestroyAiPersonSystem)}] Count: {_group.Count}");

			using var _ = _group.GetEntities(out var entities);

			var count = Random.Range(1, 6);
			while (count-- > 0)
			{
				if (entities.Length == 0)
					break;

				var index = Random.Range(0, entities.Length);
				var entity = entities[index];
				var person = entity.Person;
				Debug.Log(
					$"[{nameof(DestroyAiPersonSystem)}] Destroy {entity}");
				entity.IsDestroyed = true;
			}
		}
	}
}