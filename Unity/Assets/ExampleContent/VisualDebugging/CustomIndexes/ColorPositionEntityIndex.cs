﻿using EntitasRedux.Core.Libs.Collections;
using JCMG.EntitasRedux;
using UnityEngine;

namespace ExampleContent.VisualDebugging.CustomIndexes
{
	[CustomEntityIndex(typeof(VisualDebugContext))]
	public class ColorPositionEntityIndex : EntityIndex<VisualDebugEntity, Vector2Int>
	{
		public ColorPositionEntityIndex(VisualDebugContext context) :
			base(
				nameof(ColorPositionEntityIndex).GetHashCode(),
				context.GetGroup(VisualDebugMatcher.AllOf(
					VisualDebugMatcher.Position,
					VisualDebugMatcher.Color)),
				(e, c) => e.Position.Value)
		{

		}

		[EntityIndexGetMethod]
		public ImmutableHashSet<VisualDebugEntity> GetColorEntitiesAtPosition(PositionComponent pos)
		{
			return GetEntities(pos.Value);
		}
	}
}
