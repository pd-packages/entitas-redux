using JCMG.EntitasRedux.Commands;

namespace ExampleContent.Commands
{
	public class GameCommandBuffer : CommandBuffer
	{
	}

	public class GameManager
	{
		private readonly ICommandBuffer _buffer;

		public GameManager(ICommandBuffer buffer)
		{
			_buffer = buffer;
		}

		public void Start()
		{
			_buffer.CreateEmptyGame();
			_buffer.CreateIntGame(457);
		}
	}
}