using JCMG.EntitasRedux.Commands;

namespace ExampleContent.Commands
{
	[Command]
	public struct EmptyGameCmd
	{
	}

	[Command]
	public struct IntGameCmd
	{
		public int Value;
	}
}