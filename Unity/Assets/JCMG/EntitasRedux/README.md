# JCMG Entitas Redux

[![NPM package](https://img.shields.io/npm/v/com.playdarium.unity.entitas-redux?logo=npm&logoColor=fff&label=NPM+package&color=limegreen)](https://www.npmjs.com/package/com.playdarium.unity.entitas-redux)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Installing

Using the native Unity Package Manager introduced in 2017.2, you can add this library as a package by modifying your
`manifest.json` file found at `/ProjectName/Packages/manifest.json` to include it as a dependency. See the example below
on how to reference it.

### Install via OpenUPM

The package is available on the [npmjs](https://www.npmjs.com/package/com.playdarium.unity.entitas-redux/)
registry.

#### Add registry scope

```
{
  "dependencies": {
    "com.playdarium.unity.entitas-redux": "x.x.x"
  },
  "scopedRegistries": [
    {
      "name": "Playdarium",
      "url": "https://registry.npmjs.org",
      "scopes": [
        "com.playdarium.unity"
      ]
    }
  ]
}
```

#### Add package in PackageManager

Open `Window -> Package Manager` choose `Packages: My Regestries` and install package

### Install via GIT URL

```
"com.playdarium.unity.entitas-redux": "https://gitlab.com/pd-packages/entitas-redux.git#upm"
```

# Entitas Redux

## About

This version of Entitas Redux is a reworked version
of [EntitasRedux](https://github.com/jeffcampbellmakesgames/Entitas-Redux) with a sole focus on Unity.

## Requirements

* **Min Unity Version**: 2022.3

## Installing Entitas Redux and Getting Started

### Context creation

To create your first Context in assembly, you need to declare it using attribute.

Example:

```csharp
using JCMG.EntitasRedux;

public sealed partial class MyFirstAttribute : ContextAttribute
{
}
```

After code analysis detects your attribute, it's automatically generating all implementations for Context and Entity
classes.

### Component creation

Analyzer detects components by name of a class or base type it must end with `Component` and have one or greater
attributes

#### Simple component

Examples:

```csharp
using JCMG.EntitasRedux;

[MyFirst]
public class MyFirstComponent : IComponent
{
}

[MyFirst]
public class MySecond : IComponent
{
}
```

#### Inherited component

Examples:

```csharp
using JCMG.EntitasRedux;

[MyFirst]
public class InheritedComponent : MyFirstComponent
{
}
```