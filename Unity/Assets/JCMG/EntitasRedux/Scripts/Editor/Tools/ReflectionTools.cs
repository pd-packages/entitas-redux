using System;
using System.Collections.Generic;
using System.Linq;

namespace JCMG.EntitasRedux.Editor
{
	public static class ReflectionTools
	{
		/// <summary>
		/// Returns an IEnumerable of class instances of Types that implement V.
		/// </summary>
		/// <typeparam name="T">the Type a class must implement</typeparam>
		/// <returns></returns>
		public static IEnumerable<T> GetAllImplementingInstancesOfInterface<T>()
		{
			var objects = new List<T>();
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (var type in assembly.GetTypes()
					         .Where(
						         myType => myType.IsClass &&
						                   !myType.IsAbstract &&
						                   !myType.IsGenericType &&
						                   myType.GetInterfaces().Contains(typeof(T))))
				{
					objects.Add((T)Activator.CreateInstance(type));
				}
			}

			return objects;
		}
	}
}