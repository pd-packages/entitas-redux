using System.Collections.Generic;
using System.Linq;
using JCMG.EntitasRedux;
using NUnit.Framework;

namespace EntitasRedux.Tests
{
	public abstract class ADescribeICollector
	{
		private IContext<MyTestEntity> _context;
		private ICollector<MyTestEntity> _collector;

		[SetUp]
		public void SetUp()
		{
			_context = new MyTestContext();
			_collector = CreateCollector(_context);
		}

		protected abstract ICollector<MyTestEntity> CreateCollector(IContext<MyTestEntity> context);

		protected abstract MyTestEntity CreateEntity(IContext<MyTestEntity> context);

		protected abstract void TriggerCollect(MyTestEntity entity);

		protected abstract void RemoveTrigger(MyTestEntity entity);

		[NUnit.Framework.Test]
		public void Count()
		{
			Assert.Zero(_collector.Count);

			for (var i = 1; i <= 10; i++)
			{
				CreateEntityAndTriggerCollect();
				Assert.AreEqual(i, _collector.Count);
			}
		}

		[NUnit.Framework.Test]
		public void CollectEntityWhenReActivate()
		{
			_collector.Deactivate();
			Assert.Zero(_collector.Count);

			CreateEntityAndTriggerCollect();
			Assert.Zero(_collector.Count);

			_collector.Activate();
			CreateEntityAndTriggerCollect();
			Assert.AreEqual(1, _collector.Count);
		}

		[NUnit.Framework.Test]
		public void CollectAlreadyExistedEntityWhenReActivate()
		{
			_collector.Deactivate();
			Assert.Zero(_collector.Count);

			var entity = CreateEntityAndTriggerCollect();
			Assert.Zero(_collector.Count);

			RemoveTrigger(entity);

			_collector.Activate();
			TriggerCollect(entity);
			Assert.AreEqual(1, _collector.Count);
		}

		[NUnit.Framework.Test]
		public void DoNotCollectEntityWhenDeactivate()
		{
			_collector.Deactivate();
			Assert.Zero(_collector.Count);

			CreateEntityAndTriggerCollect();
			Assert.Zero(_collector.Count);
		}

		[NUnit.Framework.Test]
		public void ClearCollectedEntities()
		{
			Assert.Zero(_collector.Count);

			CreateEntityAndTriggerCollect();
			Assert.AreEqual(1, _collector.Count);

			_collector.ClearCollectedEntities();
			Assert.Zero(_collector.Count);
		}

		[NUnit.Framework.Test]
		public void GetEntities()
		{
			const int count = 10;

			Assert.Zero(_collector.Count);

			var list = new List<IEntity>();
			for (var i = 1; i <= count; i++)
			{
				var entity = CreateEntityAndTriggerCollect();
				list.Add(entity);
				Assert.AreEqual(i, _collector.Count);
			}

			var entities = _collector.GetEntities().ToArray();

			list.AddRange(entities);
			var array = list.Distinct().ToArray();

			Assert.AreEqual(count, array.Length);
		}

		[NUnit.Framework.Test]
		public void Enumerable()
		{
			const int count = 10;

			Assert.Zero(_collector.Count);

			var list = new List<IEntity>();
			for (var i = 1; i <= count; i++)
			{
				var entity = CreateEntityAndTriggerCollect();
				list.Add(entity);
				Assert.AreEqual(i, _collector.Count);
			}

			foreach (var entity in _collector.GetEntities())
			{
				Assert.That(list.Remove(entity));
			}

			Assert.Zero(list.Count);
		}

		[NUnit.Framework.Test]
		public void TriggerCollect()
		{
			CreateEntityAndTriggerCollect();
			Assert.AreEqual(1, _collector.Count);
		}

		[NUnit.Framework.Test]
		public void TriggerCollectMultipleTimes()
		{
			var entity = CreateEntity(_context);

			for (var i = 0; i < 5; i++)
			{
				TriggerCollect(entity);
				Assert.AreEqual(1, _collector.Count);

				_collector.ClearCollectedEntities();
				Assert.Zero(_collector.Count);

				RemoveTrigger(entity);
				Assert.Zero(_collector.Count);
			}
		}

		private MyTestEntity CreateEntityAndTriggerCollect()
		{
			var entity = CreateEntity(_context);
			TriggerCollect(entity);
			return entity;
		}
	}
}