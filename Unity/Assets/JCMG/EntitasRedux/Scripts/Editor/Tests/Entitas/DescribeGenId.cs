using EntitasRedux.Core.Libs;
using NUnit.Framework;

namespace EntitasRedux.Tests
{
	[TestFixture]
	public class DescribeGenId
	{
		[NUnit.Framework.Test]
		public void SameGenIdIsEquals()
		{
			var genId1 = new GenId(1, 1);
			var genId2 = new GenId(1, 1);

			Assert.False(genId1 != genId2);
			Assert.True(genId1 == genId2);
			Assert.True(genId1.Equals(genId2));
			Assert.AreEqual(genId1, genId2);
		}

		[NUnit.Framework.Test]
		public void GenIdWithDifferentCreationIndexNotEquals()
		{
			var genId1 = new GenId(1, 1);
			var genId2 = new GenId(1, 2);

			Assert.True(genId1 != genId2);
			Assert.False(genId1 == genId2);
			Assert.False(genId1.Equals(genId2));
			Assert.AreNotEqual(genId1, genId2);
		}

		[NUnit.Framework.Test]
		public void GenIdWithDifferentIndexNotEquals()
		{
			var genId1 = new GenId(1, 1);
			var genId2 = new GenId(2, 2);

			Assert.True(genId1 != genId2);
			Assert.False(genId1 == genId2);
			Assert.False(genId1.Equals(genId2));
			Assert.AreNotEqual(genId1, genId2);
		}
	}
}