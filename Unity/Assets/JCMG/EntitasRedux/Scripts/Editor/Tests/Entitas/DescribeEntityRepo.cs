using System;
using System.Collections.Generic;
using EntitasRedux.Core.Libs;
using JCMG.EntitasRedux;
using NUnit.Framework;

namespace EntitasRedux.Tests
{
	[TestFixture]
	public class DescribeEntityRepo
	{
		private const int ENTITIES_SIZE = 64;

		private Func<GenId, MyTestEntity> _createEntity;
		private EntityRepo<MyTestEntity> _entityRepo;

		[SetUp]
		public void SetUp()
		{
			_createEntity = id => new MyTestEntity(id);
			_entityRepo = new EntityRepo<MyTestEntity>(0, _createEntity);
		}

		[NUnit.Framework.Test]
		public void EntityRepoIndexationBeginFromZero()
		{
			Assert.True(_entityRepo.Alloc(out var entity));
			Assert.Zero(entity.Id.Index);
			Assert.Zero(entity.Id.CreationIndex);
		}

		[NUnit.Framework.Test]
		public void EntityRepoHasAllocatedEntities()
		{
			var genIds = new HashSet<GenId>(GenId.Comparer);

			for (var i = 0; i < ENTITIES_SIZE; i++)
			{
				Assert.True(_entityRepo.Alloc(out var entity));
				Assert.True(genIds.Add(entity.Id));
			}

			foreach (var genId in genIds)
			{
				Assert.True(_entityRepo.Has(genId));
			}
		}

		[NUnit.Framework.Test]
		public void EntityRepoEntitiesReusable()
		{
			var genIds = new HashSet<GenId>(GenId.Comparer);

			for (var i = 0; i < ENTITIES_SIZE; i++)
			{
				Assert.True(_entityRepo.Alloc(out var entity));
				Assert.True(genIds.Add(entity.Id));
			}

			foreach (var genId in genIds)
			{
				Assert.True(_entityRepo.Release(genId));
			}

			for (var i = 0; i < ENTITIES_SIZE; i++)
			{
				Assert.False(_entityRepo.Alloc(out var entity));
				Assert.False(genIds.Contains(entity.Id));
			}
		}

		[NUnit.Framework.Test]
		public void EntityRepoReleaseAllocatedEntities()
		{
			var genIds = new HashSet<GenId>(GenId.Comparer);

			for (var i = 0; i < ENTITIES_SIZE; i++)
			{
				Assert.True(_entityRepo.Alloc(out var entity));
				Assert.True(genIds.Add(entity.Id));
			}

			foreach (var entity in genIds)
			{
				Assert.True(_entityRepo.Release(entity));
			}

			foreach (var genId in genIds)
			{
				Assert.False(_entityRepo.Has(genId));
			}
		}
	}
}