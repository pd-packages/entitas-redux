﻿/*

MIT License

Copyright (c) 2020 Jeff Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using JCMG.EntitasRedux;

namespace EntitasRedux.Tests
{
	public interface IReactiveSystemSpy
	{
		int DidInitialize { get; }
		int DidExecute { get; }
		int DidCleanup { get; }
		int DidTearDown { get; }
		IEntity[] Entities { get; }
	}

	public class ReactiveSystemSpy : ReactiveSystem<MyTestEntity>, IReactiveSystemSpy, IInitializeSystem,
		ICleanupSystem, ITearDownSystem
	{
		public int DidInitialize => _didInitialize;
		public int DidExecute => _didExecute;
		public int DidCleanup => _didCleanup;
		public int DidTearDown => _didTearDown;
		public IEntity[] Entities => _entities;

		public Action<List<MyTestEntity>> executeAction;

		protected int _didInitialize;
		protected int _didExecute;
		protected int _didCleanup;
		protected int _didTearDown;
		protected MyTestEntity[] _entities;

		private readonly Func<MyTestEntity, bool> _filter;

		public ReactiveSystemSpy(ICollector<MyTestEntity> collector) : base(collector)
		{
		}

		public ReactiveSystemSpy(
			ICollector<MyTestEntity> collector,
			Func<IEntity, bool> filter
		) : this(collector)
		{
			_filter = filter;
		}

		protected override ICollector<MyTestEntity> GetTrigger(IContext<MyTestEntity> context)
		{
			return null;
		}

		protected override bool Filter(MyTestEntity entity)
		{
			return _filter == null || _filter(entity);
		}

		public void Initialize()
		{
			_didInitialize += 1;
		}

		protected override void Execute(ReadOnlySpan<MyTestEntity> entities)
		{
			_didExecute += 1;
			_entities = entities.ToArray();
			executeAction?.Invoke(_entities.ToList());
		}

		public void Cleanup() => _didCleanup += 1;

		public void TearDown() => _didTearDown += 1;
	}
}