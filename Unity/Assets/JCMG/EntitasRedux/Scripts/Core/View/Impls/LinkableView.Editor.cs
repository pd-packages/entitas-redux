#if UNITY_EDITOR
using UnityEditor;

namespace JCMG.EntitasRedux.Core.View.Impls
{
	public abstract partial class LinkableView
	{
		private void OnValidate()
		{
			EditorApplication.delayCall += _OnValidate;
		}

		private void _OnValidate()
		{
			if (this == null)
			{
				EditorApplication.delayCall -= _OnValidate;
				return; // MissingRefException if managed in the editor - uses the overloaded Unity == operator.
			}

			OnValidateCallback();
		}

		protected virtual void OnValidateCallback()
		{
			EditorAttackEntityLink();
		}

		private void EditorAttackEntityLink()
		{
			var component = GetComponent<EntityLink>();
			if (entityLink == component)
				return;

			var serializedObject = new SerializedObject(this);
			serializedObject.FindProperty(nameof(entityLink)).objectReferenceValue = component;
			serializedObject.ApplyModifiedProperties();
		}
	}
}
#endif